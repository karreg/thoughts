## To be handled by the manager

- [ ] Make sure Bobby is in the correct AD group
- [ ] Add Bobby to the team mailing list
- [ ] Give Bobby access to the shared calendar
- [ ] Give Bobby access to shared documents in Alfresco/Confluence/SharePoint
- [ ] Give Bobby access to shared secrets Vault path
- [ ] Give Bobby access to the Jira projects
  - [ ] Jira Support project access
  - [ ] Jira Team project access

## To be handled by the tech leader

- [ ] Give Bobby access to the GitLab/GitHub/Gitea
- [ ] Give Bobby access to the Jenkins/GitLab-CI/CircleCI
- [ ] Give Bobby access to the Artifactory/Nexus

## To be handled by the ops leader

- [ ] Give Bobby access to the platforms
    - [ ] On-Prem
        - [ ] ITG servers
        - [ ] PRP servers
        - [ ] PRD servers
    - [ ] Cloud Service Provider
        - [ ] ITG tenant
        - [ ] PRP tenant
    - [ ] kubernetes cluster
        - [ ] PRP cluster
        - [ ] PRD cluster
