
## Table of Content

* [TEMPLATE INSTRUCTIONS](#template-instructions)
* [RELEASE DESCRIPTION](#release-description)
* [PRE-RELEASE](#pre-release)
* [RELEASE](#release)
* [POST-RELEASE](#post-release)
* [ROLLBACK](#rollback)
* [POST-ROLLBACK](#post-rollback)

## Template instructions

* Set the values in the release description section
    * Release date;
    * Current version
    * Target version
* Override the default values in the release description if needed
    * Release time
    * Expected downtime

## Release description

/due YYYY-MM-DD  
Release time: 12:00  
Expected downtime: 10 minutes  
Current version: TODO  
Target version: TODO  
/label ~Release

## Pre-release

* [ ] Test version on ITG environment
  > * Set the target version on ITG environment
  > * Execute the deployment pipeline
  > * Do sanity checks

* [ ] Test version on PRP environment
  > * Set the target version on PRP environment
  > * Execute the deployment pipeline
  > * Do sanity checks

* [ ] Create a tag for this release named `Release-YYYY-MM-DD-TARGETVERSION`

* [ ] In case of new version, [create a announcement](https://corp-portal/tool/news) with a changelog digest

* [ ] Create a broadcast message in [GitLab](https://gitlab.com/admin/broadcast_messages)  
  > Background color: `#e75e40`  
  >
  > New version template:  
  > ```markdown
  > A tool update will be done on TODO at TODO. 10 minutes of downtime are expected.
  > ```
  >
  > Maintenance template:  
  > ```markdown
  > A tool maintenance will be done on TODO at TODO. A downtime may happen.
  > ```

* [ ] Plan a downtime in [Zabbix](https://zabbix)

## Release

* [ ] Executed the deployment pipeline in PROD for the release tag

## Post-release

* [ ] [Connect to the tool](https://tool) and check logs

* [ ] Check with manual human controls
    > * Execute action A and check the results
    > * Execute action B and check the results

* [ ] In cas of new version, create a broadcast message in [GitLab](https://gitlab.com/admin/broadcast_messages)  
    > Background color: `#1aaa55`
    >
    > New version template:
    > ```markdown
    > Tool has been updated. You can check the changelog [here](https://corp-portal/tool/news).
    > ```

1. [ ] Update the issue status
      > ```text
      > Release done /shrug
      > /label ~Released
      > /close
      > ```

## Rollback

* [ ] Execute the deployment pipeline in PROD for the previous release tag

## Post-rollback

* [ ] [Connect to the tool](https://tool) and check logs

* [ ] Check with manual human controls
    > * Execute action A and check the results
    > * Execute action B and check the results

* [ ] In cas of new version, create a broadcast message in [GitLab](https://gitlab.com/admin/broadcast_messages)  
    > Background color: `#1aaa55`
    >
    > New version template:
    > ```markdown
    > Tool has been rollbacked. The release will be planned again.
    > ```

1. [ ] Update the issue status
      > ```text
      > Rollbacked :'(
      > /label ~Rollbacked
      > /close
      > ```
