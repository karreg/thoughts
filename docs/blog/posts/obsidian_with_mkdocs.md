---
authors: [ karreg ]
date: 2024-04-22
categories:
  - software
tags:
  - mkdocs
  - obsidian
---
# Obsidian as MkDocs frontend

MkDocs and Obsidian are two very, very, _very_ useful softwares, and the best part is they can work together.

<!-- more -->

## The story

🤓 I am a huge fan of [MkDocs](https://www.mkdocs.org/), and especially [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/). You write content in MarkDown, and you can publish a documentation website, a blog, a presentation website, or everything at the same time. This blog is written with Material for MkDocs, after a few years as a Hugo blog. As soon as the blog feature was made available in Material, I made the switch as I find MkDocs easier to use and more versatile. I even have written a [devcontainer to use MkDocs](https://hub.docker.com/r/karreg/mkdocs-devcontainer), but this story is for another time.

Also.

😎 I am a huge fan of [Obsidian.md](https://obsidian.md/), a note application using MarkDown as backend format. It can do _a lot_ of things with _a lot_ of plugins, like synchronize your notes with a Git repository thanks to the [Git plugin](https://github.com/denolehov/obsidian-git). Obsidian won't fix your note taking issues however, because this is a problem without a single solution. But it will help a lot. Anyway, if you are using Evernote or OneNote, it's a bit different, but really worth the try.

What is the link between both you're asking? Well... It all started with a personal project.

🎲 I have been writing Tabletop RPG campaigns _for years_. Most of the time, I have written them in my head. Faster to write, easier to read. But it's limited in the sense as you can't really write a complex campaign, or share it with fellow players. So this time I have decided to write it and share it with MkDocs.

First it was using a GitBook theme, but I have switched to Material as soon as the blog feature has been made available.

Then, I have discovered Obsidian. And while looking for interesting plugins and feedbacks, I have found a community using it to _play_ their campaigns. Yes, there are also plugins like [Dice Roller](https://github.com/javalent/dice-roller). Another point was that the wiki-like features of Obsidian was a _far_ better way to write and manage the content than plain MarkDown links in Visual Studio Code.

But I still wanted to be able to publish the content as an MkDocs site, to share, ask for reviews and so on, without asking people to install software if they don't want to.

And that's how it started.

📜 The story is longer than the actual content of this article, but I find it interesting, as it shows how both tools are really complementary. One is _really good_ to publish content, and the other is _really good_ to write and manage content. Now how can you have both working together?

## Create an MkDocs site

The easiest way is to start with MkDocs. I won't dive in how it's working, but instead I will give a few hints.

### Tags limitation

Obsidian does not know how to handle spaces in tags. Even with double quotes, Obsidian does not know how to handles spaces in tags... That's a bit deceptive, but it's a common issue, and MkDocs is more the exception here.

So, don't use spaces in your tags. Go through your content and replace spaces in tags with the more common `_` character.

### Wiki links

Obsidian uses (by default, configurable) the common WikiLink syntax `[[Page]]` (or its extended version `[[the page you want to see|Page]]`). It's easier to write than MarkDown links, as you don't need a target and you let Obsidian handle it for you, and in Obsidian it's associated with completion to help you create links faster.

You can also create content from a Wiki Link, which you can't do with MarkDown links. Just create a Link to a non existing page, click the link, and the page will be created (maybe not in the right place though).

The issue is that MkDocs does not know how to handle these links, so we need a MkDocs plugin called [mkdocs-ezlinked-plugin](https://pypi.org/project/mkdocs-ezlinked-plugin/). This is a fork from another plugin called [mkdocs-ezlink-plugin](https://github.com/orbikm/mkdocs-ezlinks-plugin) that has unfixed bugs and is in practice unusable. 

Would you be surprised that the forker ([Mara-Li](https://github.com/Lisandra-dev)) is also an Obsidian user and a Tabletop RPG player? Thank you Mara-Li for answering me in the original project and pointing to your fork 🙏.

Anyway, this comes with a bit of weird configuration. Since the fork has only provided fixes, its name remains the same as the original plugin. One will work, not the other. Make sure to have the correct plugin installed.

```
# requirements.txt
mkdocs-ezlinked-plugin
```

```yaml
# mkdocs.yml
plugins:
  - ezlinks
```

> [!note]
> Obsidian can also work with standard MarkDown links. This is a bit less humanly usable, but it can ease this part. It's up to you to test both options in the `Files and links` settings section.

### Admonitions

> [!tip]
> I really like admonitions. It can both emphase published content, and help writing content in a continuous stream.

However, they don't work in the same way in Obsidian and Material for MkDocs:

```markdown
!!! info
	This is an admonition block in Material for MkDocs.
```

```markdown
> [!tip]
> This is an admonition in Obsidian.
```

There is an MkDocs plugin to handle that! This plugin is [mkdocs-callouts](https://pypi.org/project/mkdocs-callouts/) and transcode Obsidian callouts to material callouts. Let's have a look:

!!! info
    This is a MkDocs callout

> [!info]
> This is an Obsidian callout

If both callouts above looks the same, it's perfect.

```
# requirements.txt
mkdocs-callouts
```

```yaml
# mkdocs.yml
plugins:
  - callouts
```

### d2 diagrams

I have long been looking for a good diagram-as-code tooling. [d2](https://d2lang.com/) is not perfect, but it's the best fit I have found. Free not-perfect-but-good-enough rendering, and better than other tooling like Mermaid, and if you want to pay you can have a better rendering.

There is a plugin called [mkdocs-d2-plugin](https://github.com/landmaj/mkdocs-d2-plugin). I have discussed with the developer to improve the usability from a user point of view, and the experience is great now. You can embed diagrams in your markdown code, or reference d2 diagrams files as images, with optional parameters to use advanced d2 features.

Here is an example:

```d2
direction: right
Hello -> World
```

d2 diagrams can be used in Obsidian too, but, for now, the experience is more limited. We will see below.

```
# requirements.txt
mkdocs-d2-plugin
```

```yaml
# mkdocs.yml
plugins:
  - d2:
      theme: 104 # See https://d2lang.com/tour/themes
      layout: elk
      sketch: true
      pad: 10
      scale: 1.2
```

## Add Obsidian to your MkDocs site

This is the easiest part. Start Obsidian and open your MkDocs project with the `Open folder as vault`. Obsidian will add all the needed files in its `.obsidian` folder.

Just a few tweaks and it's done.
### `.gitignore` the workspace

Obsidian handles your tabs and open files in a file called `workspace.json`. If you don't synchronize your site on multiple devices with concurrent writing, it's fine. But if you do, this file will create more Git conflicts than what you are willing to handle. So... It's best to ignore it by default, just in case:

```
.obsidian/workspace.json
```
### Configure the attachment folder

Obsidian handle copy/paste of image, which is a great improvement over Visual Studio Code. But it will not handle how you want to manage your images. Everything in the same folder? A folder per blog post?

Anyway, it's probable that the default behavior won't suit your taste. I'm not sure it suits anyone's taste, because this behavior is to store images at the root directory of the vault.

Also, it's not compatible with MkDocs.

To change it, Go to the vault settings, in the `Files and links` section. My advice is to use the `In subfolder under current folder` option with a `attachments` subfolder name.

!!! warning
	If you don't set a subfolder name, the setting will be revert to another option.

You can later move images, and even rename them. Obsidian will follow changes and update the MarkDown links for you 💪.

### Local Images Plus

This plugin is a great addon to the previous point. It will handle copy/pasting image links, only to download these external images to your local vault. It comes with configuration to reduce the image file size.

### Git plugin

I use this plugin with my notes vaults so they are synchronized automatically. Install it, configure its behavior (sync frequency, merge strategy and so on), and you're done.

But this is different for published content. I want to check the result of my changes before pushing the content to GitLab and see it published right away with GitLab-CI and GitLab Pages.

I use my local devcontainer to do so (more articles to come), and since I have a running Visual Studio Code, I prefer using its Git sync interface.

But I still like to have the Git plugin installed, just in case. I just disable the auto-sync feature (it's disabled by default).

And that's it. You can now use Obsidian as a frontend to your MkDocs site.

### D2 plugin

This plugin give the ability to write and compile D2 diagrams in Obsidian, but _only in embedded mode_. This is far more limited than the `mkdocs-d2-plugin` but it's still better than nothing. I have tried having a discussion on their Discord, but no reply for now...

What is painful is that d2 files in VSCode have a better experience than embedded code, and it's the opposite in Obsidian 😢.

Anyway, here is a small diagram:

```d2
direction: right
Hello -> World
```

And here is the experience in Obsidian:

![D2 diagram in Obsidian](obsidian_with_mkdocs/d2_in_obsidian.png)

## Limitations

A few useful features are incompatible between Obsidian and MkDocs, and more specifically Material for MkDocs.

### MkDocs content plugins

More generally, MkDocs plugins related to content aren't handled by Obsidian without specific plugins.

It applies to plugins like `mkdocs-include-markdown-plugin`, `mkdocs-macros-plugin`, `mkdocs-drawio-exporter` among the plugins I usually use. But any plugin that handles non _standard_ (for what it could mean) markdown will be broken in Obsidian.

### More Obsidian customization

You can check Obsidian plugins for more useful features. I like the `Emoji Shortcodes` plugin, and you can have the same feature in Visual Studio Code with the `emojisense` extension.

You can change the default theme (which is great) by community themes (which are great too). I personally use [Catpuccin](https://github.com/catppuccin/catppuccin), because it exists for both Obsidian and Visual Studio Code, and sharing the theme is a  further improvement here.

## Conclusion

Obsidian is a great frontend to manage MkDocs site content, but don't expect it to be perfect.

It's _almost_ perfect when you use vanilla MkDocs, or Material for MkDocs, but a lot of plugins or Material features _are not_ supported by Obsidian.

It's up to you to see if you want to live with both tools depending on your ponctual need (I do), or if you want to stick to an (already great) Visual Studio Code environment.
