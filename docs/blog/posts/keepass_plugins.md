---
authors: [ karreg ]
date: 2019-10-01
categories:
- software
- keepass
tags:
- keepass
---

# KeePass Plugins

In the previous article, we have had a good look at what Keepass can do on its own. But Keepass has [a lot of plugins](https://keepass.info/plugins.html)! Let's see a few more possibilities...

<!-- more -->

## Sourceforge update checker

A lot of plugins are using sourceforge as source code repository. The issue is sourceforge is not providing an easy URL system to give direct access to a file.

Why is that an issue? Keepass provides an update checker, that checks keepass updates, but also plugin updates, based on the content of a specific file.

![Update checker](keepass_plugins/keepass_plugins-01.png)

This update checker won't work with plugins hosted on Sourceforge. The only goal of `Sourceforge update checker` is to provide update information for plugins hosted on sourceforge.

You can get it [on Sourceforge](https://sourceforge.net/projects/kpsfupdatechecker/).

## Auto-type search

In the previous article, we talked about auto-type. But what happens when auto-type is not configured for an application? And what if the website does not provide enough data to configure auto-type?

> For example, the news website [NextInpact](https://www.nextinpact.com/news/108076-donnees-personnelles-facebook-ecope-dune-penalite-5-milliards-dollars.htm) does not include its own name in the web page title, meaning we can't configure an auto-type entry for this website.

Here comes Auto-type search...

In case of no matching auto-type entry, Auto-type search will open a form to search for existing entries to use. You can then type `ENTER` to auto-type the found entry, or type `SHIFT+ENTER` to direclty open auto-type configuration for the selected entry.

![Auto-type search](keepass_plugins/keepass_plugins-02.png)

This is a must have, and this is **the one feature** that prevents me from switching to KeepassXC.

You can get it [on Sourceforge](https://sourceforge.net/projects/autotypesearch/).

## KeeAutoExec

KeeAutoExec provides the possibility to automatically open a database from another database.

This is a great feature to segregate your passwords between multiple files:

* One file for your private personal passwords
* One file for your passwords shared with your family (public taxes service, children accounts...)
* One file for your private professional passwords

The only strategy here is to use a *more to less secure/private* opening chain. For example, you may want to open your shared database from your personal database, and not the opposite since anyone that has access to the shared database would also have access to your personal database...

For each database you want to automatically open, you have to create an `AutoOpen` folder in the database, with the following values:
* `Password`: Password to open the database
* `URL`: Path to the database file, either relative or absolute (including cloud path, we will see that later). If the child database is stored besides the master database, the value will be something like this: `{DB_DIR}/Child.kdbx`

And that's it. Of course there are variations, but they are not discussed here.

You can prevent the database from auto-opening by setting the entry as expired. You can still open the database with the `File -> Open (KeeAutoExec)` menu.

You can also auto-open database only in a specific context, for example your professional database on your professional computer only. This is configured by using the `IfDevice` custom field. In the value, you can specify either:
* To open the dabatase on a specific computer. For this purpose, add the computers names (see below) separated with commas
* To *not* open the database on a specific computer. For this purpose, add the computers names (see below) prefixed with a `!`, and separated with commas

The computer name on Windows is the netbios name. You can get it through `Control Panel -> System and Security -> System`

![Netbios name](keepass_plugins/keepass_plugins-03.png)

> On Linux, this is the hostname, given by the command `hostname`.

There is more optional configuration for these entries. Full documentation is provided in the archive containing the plugin.

Here is a standard entry used to open a database besides the master database:

![Netbios name](keepass_plugins/keepass_plugins-04.png) ![Netbios name](keepass_plugins/keepass_plugins-05.png)

* `locPassword` and `locUserName` are defined as Anonymous to work with the KeeAnywhere plugin that allows to open database from a cloud provider (see further in the article)
* `Focus` with `Restore` value will set the focus back to the master database when the child database is loaded
* `IfDevice` has already been explained
* The path is using `{DB_DIR}` to reference the directory containing the master database

You can get it [on KeePass website](https://keepass.info/plugins.html#keeautoexec).

## KeePassQuickUnlock

To be able to use your database entries, you need to have it open. But leaving your database open all the time is not really secure.

On the opposite, you don't want to type your master password every five minutes because of an misconfigured SSO that requires to login every five minutes...

This is where KeePassQuickUnlock comes handy. It allows you to define and use a shorter, *less secure*, password to quickly lock and unlock your database. If this quick unlock password is wrongly typed, even once, the full password will be required to open the database. So this is a fairly good trade-off where you decrease security a bit to in fact increase it greatly...

This plugin has a specific configuration panel in `Tools -> Options -> QuickUnlock`.

![KPQU Configuration](keepass_plugins/keepass_plugins-06.png)

As advised, you should use `'QuickUnlock' Entry only`. The main option here is the time range after which a QuickUnlock key gets invalidated: the longer the less secure. The shorter the less usable. It's up to you to define how much you want to use the full password every day. If your database gets locked after 10 minutes, and the time range is set to 50 minutes, it means you may have to type your full password every hour (if you need a password every hour).

Once the configuration is set, all you need is an entry called `QuickUnlock` at the root level of your password database. The `password` field will be your quick unlock password... Don't be too much predictable.

![KPQU Entry](keepass_plugins/keepass_plugins-07.png)

Of course, this plugin makes sense only if your database is set to be locked after a period of inactivity (or session is locked, great for corporate use). This can be set up in `Tools -> Options -> Security`. Here is a somewhat secured setup.

![Lock settings](keepass_plugins/keepass_plugins-08.png)

Note that it can sometimes have side effects with children databases. In this case, I configure Quick Unlock only on the master database.

You can get it [on GitHub](https://github.com/JanisEst/KeePassQuickUnlock).

## Favicon Downloader

This nice-to-have plugin will get the favicon of the URL defined in any entry, and set it as icon for this entry... It makes entries easier to read.

To use it, you can right-click any entry and select `Download favicons`. Or you can globally grab icons with the menu entry `Tools -> Download Favicons for all entries`.

![Favicons Downloaded](keepass_plugins/keepass_plugins-09.png)

You can get it [on SourceForge](https://sourceforge.net/projects/keepass-favicon/).

## KeeAnywhere

This plugin offer the possibility to access a database in a cloud storage.

> We may discuss the risks about storing your database in a cloud storage, and there are risks. However, the file is encrypted. Preferably strongly encrypted (see first article for that matter). So the biggest risks are that the encryption algorithm, and/or the encryption implementation, contain a vulnerability. To motigate the risk, you could also use a cloud storage provider that encrypt your content, or add your own encryption layer... This is not discussed here, as this is a far more advanced usage, and it's not the subject of this article. On the other side, cloud storage offers another layer of safety over your precious database.

Cloud providers already offer the option to synchronize content on your computer, so why bother using a plugin for that?

The reason is that the way KeePass is saving the file may cause issues with the cloud storage synchronization system. Keepass will save your content to a temporary file, delete the original file, and rename the temporary file. If the synchronization is fast enough so syncing begins while files are being written, you may end up with `can't save database` error. In this case, you have to save to a new file each times it happens. At the time I was using this method, I ended up with a `MyDatabase.6.kdbx` file. It happens even more often when you use the same file on multiple devices, because the synchronization system in Keepass make this use case more likely to happen.

You can mitigate this issue by using a file per device (let's say `MyDatabase.DeviceA.kdbx` and `MyDatabase.DeviceB.kdbx`), with a master file (`MyDatabase.kdbx`) and synchronize you device file with master file every once in a while. This can still happen though.

That's why this plugin is interesting: it just won't mess up your data by depending on the cloud provider synchronization agent.

What you need is to connect your account in the `Tools -> KeeAnywhere Settings` menu, and you will be able to open a database with the `File -> Open -> Open from Cloud Drive` menu. The database will be synchronized when you open it, and when you save it. If the drive is not available, you will be able to use a local cache, and there is even an option to keep a history of database if your cloud provider do not handle it natively.

![KeeAnywhere Settings](keepass_plugins/keepass_plugins-10.png)

> This extension is the first (and only) one in this list that does not work with Linux. There is a [long running issue](https://github.com/Kyrodan/KeeAnywhere/issues/18) for that. The main reason is that it depends on a web view for connecting the cloud provider, but this web view is not available in Mono.

You can get it [on GitHub](https://github.com/Kyrodan/KeeAnywhere/releases).

## KeeAgent

This is probably the most technical plugin in this list. KeeAgent is working as an ssh agent, you don't need to copy your private keys everywhere anymore!

To work with KeeAgent, you have to create an entry for each of your ssh keys. The `Password` field will contain the key Passphrase. The key will be added to the entry as an attachment, and the last tab will let you configure KeeAgent for this key.

![ssh entry 1](keepass_plugins/keepass_plugins-11.png) ![ssh entry 2](keepass_plugins/keepass_plugins-12.png) ![ssh entry 3](keepass_plugins/keepass_plugins-13.png)

Once your keys are configured, you can check that they are correctly loaded in the KeeAgent window in `Tools -> KeeAgent`.

![KeeAgent Loaded keys](keepass_plugins/keepass_plugins-14.png)

> Sometimes servers won't let you attempt connection with more than one key. In this case you can use URL Overrides to associate a specific key to a specific server. See [KeeAgent documentation](https://keeagent.readthedocs.io/en/stable/usage/tips-and-tricks.html#creating-url-overrides-to-open-a-connection).

> KeeAgent is working natively on Linux. However, integration on Windows is not direct and you need to follow [KeeAgent documentation](https://keeagent.readthedocs.io/en/stable/usage/tips-and-tricks.html#using-keeagent-on-windows) to integrate with Cygwin or MSYS.

You can get it [on KeeAgent website](https://lechnology.com/software/keeagent/).
