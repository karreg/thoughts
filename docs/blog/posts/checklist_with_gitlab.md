---
authors: [ karreg ]
date: 2021-11-25
categories:
    - release management
tags:
    - checklist
    - gitlab
---

# How to manage checklists with GitLab

This article is a specialized, shorter version of the [How to manage releases with GitLab](release_management_with_gitlab.md) article.

GitLab issue templates can help with more than Release procedures. In this case, we want to manage checklists.

<!-- more -->

There are many useful shared checklists for a team, like:

* A vacation checklist, to make sure you don't forget anything before going away for some time so there is no havoc
* A regular lunch at the local restaurant, so everyone can check if he comes along

But the most valuable checklist, the one every team struggles to create, is the _onboarding_ (and _outboarding_) checklist.

What is to be done when someone arrives in the team?

_Checklist!_

You will need something like this, where we call every new member Bobby. Copy-pasta this code in your repository in `.gitlab/issue_templates/onboarding.md`.

```markdown

## To be handled by the manager

- [ ] Make sure Bobby is in the correct AD group
- [ ] Add Bobby to the team mailing list
- [ ] Give Bobby access to the shared calendar
- [ ] Give Bobby access to shared documents in Alfresco/Confluence/SharePoint
- [ ] Give Bobby access to shared secrets Vault path
- [ ] Give Bobby access to the Jira projects
  - [ ] Jira Support project access
  - [ ] Jira Team project access

## To be handled by the tech leader

- [ ] Give Bobby access to the GitLab/GitHub/Gitea
- [ ] Give Bobby access to the Jenkins/GitLab-CI/CircleCI
- [ ] Give Bobby access to the Artifactory/Nexus

## To be handled by the ops leader

- [ ] Give Bobby access to the platforms
    - [ ] On-Prem
        - [ ] ITG servers
        - [ ] PRP servers
        - [ ] PRD servers
    - [ ] Cloud Service Provider
        - [ ] ITG tenant
        - [ ] PRP tenant
    - [ ] kubernetes cluster
        - [ ] PRP cluster
        - [ ] PRD cluster

```

Here is the result in GitLab, when Tommy gets on the team. When he leaves, you need to create a new issue, and execute the opposite actions:

![GitLab checklist](checklist_with_gitlab/gitlab-checklist-01.png)

Of course this list is probably incomplete, since every team and every company works differently. It may be more or less complex, and its complexity may also be a good way to find room for improvement and automation.
