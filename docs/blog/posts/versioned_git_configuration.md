---
authors:
  - karreg
date: 2023-06-26
categories:
  - system
  - linux
tags:
  - versioned_devenv
  - git
---

# Versioned git configuration

git is now providing a way to include files, time to add a small article on that feature to the versioned devenv series.

<!-- more -->

Since version 1.7, git supports [include directives](https://git-scm.com/docs/git-config#_includes).

You can create your own, versioned configuration:

```bash
$ ll scripts/gitconfig/

total 104
drwxr-xr-x 2 karreg karreg  4096 2024-06-26 15:12 .
drwxr-xr-x 8 karreg karreg  4096 2024-06-26 14:48 ..
-rw-r--r-- 1 karreg karreg  2348 2024-06-26 14:58 .gitconfig
```

There is a single file because git configuration is not that big or complex, but you can use as much files as you want.

I use this setup mainly to configure proxies based on the git server I work with:

```
# My GitLab instance
[http "https://my.gitlab.instance"]
	proxy = my.proxy.fqdn:8080

[credential "https://my.gitlab.instance"]
	helper = store
```

Once the files are created, you can reference them in the `~/.gitconfig` file. Unfortunately, you will need a directive for each file:

```bash
[include]
	path = ~/scripts/gitconfig/.gitconfig
```

With this setup, custom git configuration is now versioned, allowing me to:

* go back in time in case of configuration mistake
* keep my configuration even if my computer is dead
* synchronize this configuration over multiple computers, if needed
