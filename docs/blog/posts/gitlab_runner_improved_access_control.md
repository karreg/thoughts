---
authors: [ karreg ]
date: 2022-02-18
categories:
    - development
tags:
    - gitlab
    - access_control
    - security
---

# GitLab runners: somewhat more secured access control

We have seen previously [that runners were great, but still not great enough for some corporate environments](gitlab_runner_security_risks.md), and [how we could improve the registration process](gitlab_runner_improved_registration.md). We will now see how we can improve things for registered runners.

<!-- more -->

!!! note
	This (series of) article(s) is a rewrite of [a previous article written in october 2020](gitlab_runner_api_proxy.md).

!!! articles
    _Here is the updated list or articles, in order:_

    * _[GitLab runners security risks](gitlab_runner_security_risks.md)_
    * _[GitLab runners: more secured registration](gitlab_runner_improved_registration.md)_
    * _[GitLab runners: somewhat more secured access control](gitlab_runner_improved_access_control.md)_

## Job request protocol analysis

### Early analysis

Sticking with the previous idea, the first step was to analyze the gitlab runner communication for registered runners, with the same wireshark solution.

And here is what we can find.

We have previously analyzed and validated the registration process.

![_Register request content_](gitlab_runner_improved_registration/gitlab_runner_improved_registration_02.png)

This _authentication token_ can be found in the runner configuration file.

```toml
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "docker-runner-direct"
  url = "http://gitlab/"
  token = "_2x24K1frAFiL1gPm_aW"    <-- AUTH TOKEN
  executor = "shell"
```

And that's what you can use to [clone your runner, anywhere and as many times as you like](gitlab_runner_security_risks.md#access-to-authentication-token).

So we can expect to see the authentication token in the requests, and that's indeed what we see:

![`POST` requests from the runner on `/api/v4/jobs/request` to fetch jobs](gitlab_runner_api_proxy/gitlab_api_proxy-07.png)

That's something un-documented in GitLab API documentation, but it can be found in the [gitlab-runner source code](https://gitlab.com/gitlab-org/gitlab-runner/-/blob/master/network/gitlab.go#L336).

Let's see the content of these requests:

![`token` and `last_update` values in the runner request](gitlab_runner_api_proxy/gitlab_api_proxy-08.png)

Just behind the _authentication token_, called `token`, we can find a intringuing `last_update` value.

### `last_update` analysis

Following my intuition, I made a link between this value and the _Last contact_ value, visible in the GitLab runner administration dashboard.

<!-- TODO Insert screenshot -->

Just to make sure, I checked how often and when the value changed, so I kept wireshark running for 2 hours, and checked the change on 2 runners. The _last update_ value in the requests indeed changes when the _Last contact_ value changes in GitLab. We have our link.

Here is what is saying the [GitLab documentation](https://repository.prace-ri.eu/git/help/user/admin_area/index.md#administering-runners):

_Last contact: Timestamp indicating when the GitLab instance last contacted the runner._

OK. That's weird, because from what I know, runners contact GitLab, not the opposite. But it could mean that GitLab generates the value. That would make sense. If that's the case, then GitLab should give this value to the runner. Let's check the response, just before a `last_update` value change.

Let's filter on the `token` value, and colorize the `last_update` value. And let's have a look at the response to the last request before the `last_update` change.

![Let's see what is in the GitLab response to the runner request](gitlab_runner_api_proxy/gitlab_api_proxy-09.png)

And here it is. The `last_update` value coming from GitLab. Further checks tell us that is happens only when the value has changed.

![`last_update` value in GitLab response to runner request](gitlab_runner_api_proxy/gitlab_api_proxy-10.png)

That's what we were looking for, at least to detect duplicated runners. Here's my idea:

_If the `last_update` value comes from GitLab, as response to a job request, a cloned runner should not be aware of the new value. Hence it should use the old value in its next request, meaning we should be able to see discrepancies in the sequence._

Let's find out about this theory.

I have started another capture, duplicated a runner, and left everything running for more than one hour. I have executed a few GitLab-CI jobs on these runners. Then, I have colored every different `last_update` value for the same (authentication) `token` value in the capture. And here is what we can find.

First thing, there's an isolated `last_update` value. It happens when the duplicated runner is created. It will then receive the correct value from GitLab, and everything will look normal again.

![`last_update` disruption on first access from duplicated runner](gitlab_runner_api_proxy/gitlab_api_proxy-11.png)

Second, when the `last_update` has changed after a while (it looks like it's updated every 1 hour when there's no CI activity), we can see that one runner comes with the old value. It will then receive the correct value from GitLab, and everything will be back to normal.

![`last_update` disruption with duplicated runners](gitlab_runner_api_proxy/gitlab_api_proxy-12.png)

Last, it looks like the `last_update` value is changed after a job execution, and we can see the same behavior.

![`last_update` disruption after a job execution](gitlab_runner_api_proxy/gitlab_api_proxy-13.png)

It looks like duplicated runners can be detected that way, and that something can be done, like temporary, or definitive ban of the authentication token, or even force unregistering of these runners in GitLab.

Since this behavior happens because of a race condition, there is still a risk that this pattern does not appear each time the `last_update` value changes. However, if we manage incoming requests, we will be able to intercept replies from GitLab, and get the correct `last_update` value. No more race condition.

Let's see what we can do with that.

## Access control logic

Here is the security issue we can work on:

!!! info
    Issue 2: Authentication token is easily accessible

Since there's nothin we can do about that, maybe we can mitigate the issue. We could detect `last_update` sequence disruption and block requests associated with this _authentication token_.

### Use case 1: access denied for an authentication token

This is the easiest one:

!!! rule
    As a gateway  
    Given auth_token AAA is blocked  
    When I receive a job request with auth_token AAA  
    Then I reject the request  

### Use case 2: standard access control

This use case is the standard one:

!!! rule
    As a gateway  
    Given auth_token AAA is not blocked  
    Given last_update value for AAA is XXX  
    When I receive a job request with auth_token AAA and last_update XXX  
    Then I forward the request to GitLab  
    Then I get the reply from GitLab  
    Then I forward the reply to the runner  

### Use case 3: manage last_update memory

!!! rule
    As a gateway  
    Given last_update value for AAA is XXX  
    Given I got the reply from GitLab  
    When last_update value has changed to YYY in GitLab reply  
    Then I store the last_update value YYY associated to auth_token AAA  
    Then I forward the reply to the runner  

### Use case 4: manage last_update disruption

!!! rule
    As a gateway  
    Given last_update value for AAA is YYY  
    When I receive a job request with auth_token AAA and last_update is not YYY  
    Then I block the auth_token for some time  
    Then I reject the request  

### Use case 5: empty last_update

Flash forward: in a near future, we have discovered that there's another use case that was considered a runner duplication, but that is not: empty last_update value.

In case of runner duplication, we have seen that this happens. But it also happens when:

* A legimitate runner is registered
* A runner restarts

We have decided that we should let these request go through, since we can't tell the difference between a legitimate runner and a duplicated runner.

!!! rule
    As a gateway  
    Given auth_token AAA is not blocked  
    When I receive a job request with auth_token AAA and last_update is empty  
    Then I forward the request to GitLab  
    Then I get the reply from GitLab  
    Then I forward the reply to the runner  

## Implementing the control logic

We are using the same architecture as for register control.

![_Gateway architecture_](gitlab_runner_improved_registration/gitlab_runner_improved_registration_04.d2){target="cache_usage"}

Redis lifetime management will be useful for temporary bans. If the issue is not fixed by the time the ban is lift, a new ban will occur for the same reason.

This code will take care of use case 1:

```python
@app.route('/api/v4/jobs/request', methods=['POST'])
def runner_job_request():
    """ Captures runner job request """

    auth_token = __extract_job_request_data(request)

    if cache.is_banned(auth_token):
        return "Runner is banned", 504

    ...
```

We are sending a `504` error (Gateway Timeout) instead of `403` (Unauthorized) because the runner will stop requesting GitLab after a few `403` replies. This way, even after a few hours, the runner will keep requesting GitLab and the ban won't break links between runners and GitLab.

We then add code to handle use case 4 and 5. `__should_be_banned` will take care of checking if the value is empty or different from the expected value.

```python

    ...
    
    # The expected last_update value is the latest sent by GitLab
    expected_last_update = cache.get_last_update(auth_token)

    if __should_be_banned(last_update_input, expected_last_update):
        # Hence, the auth_token is banned (all runners are banned, even the legit. one)
        cache.ban(auth_token, BAN_PERIOD)
        return "Runner is banned", 504

    ...
```

after these checks, we send the request to GitLab, and we get the fresh `last_update` from the response when the value changes, and we store it (Use case 3).

```python

    ...

    # We send the request to GitLab. Since it's a separate service, it may be down, or something else may happen.
    # In this case, we send a 504 error that is adapted to this case.
    try:
        gitlab_response = __get_gitlab_response(request)
    except requests.exceptions.ConnectionError as ex:
        return __create_error_response("GitLab is unavailable", __get_ip_adress(), ex)

    # We extract the last_update value sent back by GitLab.
    # If it's the same as in the request, the value won't be here (None).
    # It will be here only when the value changes. In this case we have to update the cache
    last_update_output = __extract_last_update(gitlab_response)
    if (last_update_output is not None and
        last_update_output != expected_last_update):
        cache.set_last_update(auth_token, last_update_output)

    ...

```

Then we forward the reply to the runner (Use case 2).

```python
    ...

    # We create the response to send back to the runner
    response = __create_forwarded_response(gitlab_response)

    ...
```

I won't give the helper methods as they are simple. I hope I will be able to publish the code directly as opensource software, even if it's only for documentation purpose. But they are really straightforward. It's all about getting data from requests and replies.

With this code, I was able to detect and block duplicated runners. Unfortunately, new duplicated runners won't be detected straight away. But in the best case, it won't execute any job for one hour, and will be detected when the value changes. In the worst case, a job will be executed right away, and the detection too.

## Performance impact

We have analyzed performances for this access control part. It's doing more than what is described in this article (see further articles), but the longest part is about what we have seen here.

![_Custom time and GitLab RTT_](gitlab_runner_improved_access_control/gitlab_runner_improved_access_control_01.d2)

`overhead = custom_time / gitlab_rtt`

Since our pilot is running for months at the time I write this article, I have applied this formula in the following context:

* GitLab is running in production on a single server
* Redis is running in a container locally
* The gateway is running locally
* The gateway is behind GitLab's nginx

The obtained result in this context is the following:


| Measure          | Value  |
|------------------|--------|
| Mean custom_time | 1.8 ms |
| Mean gitlab_rtt  | 2.4 ms |
| Mean overhead    | ~75%   |

The impact is quite noticeable, but not that high.

Also, there's still space for optimization since we query the cache multiple times.

## How GitLab could implement it

GitLab is already using Redis as a cache, so having it working, even in a high available architecture, should not be an issue.

Here, there's not much UX needed. Only configuration (but it's probably better defined in the gitlab.rb configuration file), and surely an associated status in the runner dashboard, but that's all.

I would make an improvement in the protocol though, so instead of sending `504` error to the runner, that is undebuggable, I would send a structured reply, so the runner can log it, and describe the status with the `gitlab-runner verify` command.

## Next articles

In the next article, we will see what we can do about runner management.
