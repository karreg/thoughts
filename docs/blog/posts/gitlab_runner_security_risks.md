---
authors: [ karreg ]
date: 2021-01-18
categories:
  - development
tags:
  - gitlab
  - access_control
  - security
keywords:
  - gitlab_runners
---

# GitLab runners security risks

GitLab is a great piece of tooling chain, even a full chain if your needs fit. But there's something GitLab is not good at all : business use cases. Of course, there is a Entreprise version, and of course it comes with some _Business oriented_ use cases, but they are mostly _end user_ use cases, not IT uses cases.

The core business of GitLab is opensource projects, and it makes sense since it came out as a GitHub competitor. Every feature (sometimes meaning lack of) makes sense when looking at it from the opensource perspective.

And yes, we are talking about GitLab runners, especially how they are handled by GitLab.

<!-- more -->

!!! note
	This (series of) article(s) is a rewrite of [a previous article written in october 2020](gitlab_runner_api_proxy.md).

!!! articles
    _Here is the updated list or articles, in order:_

    * _[GitLab runners security risks](gitlab_runner_security_risks.md)_
    * _[GitLab runners: more secured registration](gitlab_runner_improved_registration.md)_
    * _[GitLab runners: somewhat more secured access control](gitlab_runner_improved_access_control.md)_

## Disclaimer

This article is about security risks. Nothing else. 

_Yes_, good practices can avoid such risks to appear. But good practices are not always applied, and not by everyone.
_Yes_, they are risks, they don't _all_ appear _in any company_.
_Yes_, GitLab is not obliged to take care of them, especially since it is opensource first oriented.
_Yes_, GitLab runners are great.

## What are runners

Runners are basically CI/CD agents. They take jobs from GitLab, execute them, sends the result back to GitLab. All of that in _https_. Nothing new or complex here.

![No seriously, nothing new, nor complex](gitlab_runner_api_proxy/gitlab_api_proxy-01.d2)

Runners are great though, they feel like a modern take on CI/CD, far from the pioneers that have, to my feeling, aged. And the most useful feature they come with is the ability to execute jobs inside a container, making the CI/CD development and maintenance way, way easier (though it has some drawbacks too).

There is a lot of options when we are talking about configuring runner behavior and technical stack. However, here are 2 options, that are enough to explain issues with runners:

* The shell executor is like a good old Jenkins slave. You set up your computer, install what you will need, and register it to GitLab. Jobs will then have access to your local storage (in a dedicated folder tree), and the tools that are installed on it. It will require management to updade the tools.
* The docker executor offers job execution in a container, which is great for shared runners. Upgrading the runner itself is easy, there's no tooling upgrade, and project can choose their tools by selecting the docker image they need, or create it if needed. You can even run the runner executable in a container, but it comes with long .

The problem is GitLab offers no way to control runners access in a corporate environment, even in the entreprise version.

## How runners work

### Registration

To register your runner, you need a _register token_. It can be an instance wide token (available to administrators), a group wide or a project token (available to owners and maintainers), and can be fetched from the GitLab UI.

You register your runner with this token, using a command on the runner itself. GitLab will accept the registration is the token is valid, and reply with an _authentication token_, that the runner will store and use when requesting GitLab to identify itself.

![Enrollment process](gitlab_runner_api_proxy/gitlab_api_proxy-02.d2)

* The register token is stable, meaning it can be used as long as you want, for as many runners as your want, until it is revoked
* A revoked register token will block any registration with that token, but won't block runners that have been registered with it before it was revoked. This is important, as the revoke action is not really explaining what are the impacts (and that's part of one problem here)
* Once a runner is registered, it forgets the register token, and only keep the authentication token.

Here is a extract of the runner configuration stored in a file. The `token` value is the _authentication token_.

```toml
[[runners]]
  name = "my-runner"
  url = "http://gitlab/"
  token = "_2x24K1frAFiL1gPm_aW"
  executor = "shell"
```

### Job execution

A registered runner will query GitLab with its authentication token. GitLab will reply with a job to execute, if it exists. Only jobs that match the runner criteria will be given:

* The job must be in the scope of the _register token_
    * For an instance token, it can be any job from any project, provided that the project allowed execution on these runners
    * For a group token, it can be any job from any project in this group
    * For a project token, it can be only a job from this project.
* The job must match the runner capabilities. These capabilities are defined at registration, but we won't discuss them here.

## Security issues

Before discussing the security issues, let's discuss the impacts.

* A corrupted runner will have access to the source code (limited to the scope of the runner).
* A corrupted runner will have access to the variables, including ssh keys, API tokens, passwords, or any secret used in the jobs.
* A corrupted runner has an access to the GitLab instance through artifact uploads, and may lead to further corruption.

This is traditional stuff, but it can stay undetected for a very long time. Now let's see what are the security issues.

### Access to registration token

We have seen this behavior before. Registration token is available only to owners and maintainers. The logic behind is to keep this information available only to trusted, involved people. And while it makes sense for opensource projects, it's not the case in corporate environment.

In corporate environments, owners and maintainers are not (only) trusted or involved people. They are team or project managers, product owners... People that may be distant from technical matters like CI/CD or data security.

In this case, who really has access to the registration token? Almost anyone. Because it's needed.

Team developers and/or ops have to implements CI/CD, and that means creating a runner and registering it. They _legitimately_ need this token though they don't have the access right. And that's the first security breach, since this token can be used as much as one want until it is revoked.

The other issue related to this token is team membership management. It's quite widespread to see people keeping their project membership despite their leaving. If this GitLab instance is available from internet, with no account lifetime management - _which is, by the way, **NOT** the duty of GitLab_ - people may still have access to register tokens for months, if not years.

!!! info
    Issue 1: Register token is long living and easily shared

### Access to authentication token

We have seen that the authentication token is used to identify the runner in GitLab, and that this token is in the configuration file of the runner. But what happens when someone copy this authentication token to another runner? Well, they are two, but for GitLab they are one. No difference.

![Runner cloning flaw](gitlab_runner_api_proxy/gitlab_api_proxy-03.d2)

And the issue is the same as for register token. When you are in a team, tasks are shared, so almost everyone has access to the runner, for example to update the system, the tooling, to tweak the configuration, and so on. Everyone can have a legitimate access to the authentication token.

[GitLab is aware of this issue for at least 3 years](https://gitlab.com/gitlab-org/gitlab-runner/-/commit/b78c7ac66b625fa6d0c5247e5b4afa84a9f5a089). I think this is something that is not top priority due to the opensource target.

!!! info
    Issue 2: Authentication token is easily accessible

### Registered runners review

There a two possible actions when reviewing existing runners.

The first action is to reset the register token. In this case, this token can't be used anymore to register runners. But it does not revoke runners that have been previously registered with it.

The first issue with this action, is that impacts are not clearly explained in the UI. What would a project manager do with that? Risk to reset the register token and break CI/CD? Or leave it like that to make sure CI/CD won't break? It will surely be the second option.

![_Are you sure you want to do that thing you don't know what it does?_](gitlab_runner_security_risk/gitlab_runner_security_risk-01.png)

!!! info
    Issue 3: Resetting the register token is not clear

The other issue, is that it is a manual option. This is something you have to think about. And you have to think about it, even though you are not part of the developers, and not necessarily in the tech field.

!!! info
    Issue 4: There is no option to have time limited register tokens

Managing the registered runners is an issue too. You can review the registered runners, and remove the runners you won't want to use anymore. This is a maintenance task that _should_ be regularly done. But, will it be done? Maybe. Probably not. At least not until it's a mess.

![Runners can be removed one by one](gitlab_runner_security_risk/gitlab_runner_security_risk-02.png)

_The issue is not with GitLab itself_. Everything is done in GitLab to ease runner management. You can name your runner, associate tags used to execute jobs, but also to simplify the runner management. But this force is a weakness too. It's far too easy to add _my-runner_ without any tag, and multiple times, on multiple servers, or even developper computers... You may not be able to tell which runner is legitimate or not, which one should be kept, and which one should be removed without breaking CI/CD. Chances are you will just leave them be.

!!! info
    Issue 5: Runner fleet management can quickly be a mess (because it's so easy to set up)

### Runner management

The last set of issues concern the runner itself, which is an important part of the security chain. Fraudulent access that could lead to:

* Data leak, since source code is downloaded on the runner, even though it's temporary
* Infrastructure breach

There are many ways to access a runner, like system or software vulnerabilities, reverse shell... But since I'm not an expert, I won't go down this path.

The only way to keep these risks as low as possible is to keep the runner maintained in term of OS and software updates. And a runner can be deprecated either not not being maintained - by lack of time, will, or by fear to break things - or because access to the runner have been lost. And GitLab is not enforcing runner upgrades more than the compatibility matrix between gitlab-runner version and GitLab version.

Of course, this would not be an issue if such runner are regularly removed, but we have seen before that this is not always the case.

!!! info
    Issue 6: Unmaintained runners are a security hole

## Next articles

In [the next articles](gitlab_runner_improved_registration.md), we will see _if_ and _how_ we can mitigate these risks (spoiler alert, we can, by enforcing controls).
