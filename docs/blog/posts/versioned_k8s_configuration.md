---
authors: [ karreg ]
date: 2019-03-06
categories:
- system
- linux
tags:
- versioned_devenv
- kubernetes
---

# Versioned kubernetes configuration

Kubernetes configuration is... well... a hell. Especially when you have multiple clusters to manage.

<!-- more -->

The configuration can be in json or yaml format. I will use yaml format here, but it should work with json too, or maybe not.

The Kubernetes configuration can (very) quickly become not human readable. Being able to split it is really good news. The kubernetes documentation explaining how it works is [here](https://kubernetes.io/docs/concepts/configuration/organize-cluster-access-kubeconfig/#the-kubeconfig-environment-variable), however, here is a more concrete example.

I will not dive in the configuration file format itself, just the file organization.

I have a ~/scripts directory, containing a kubernetes subdirectory where my files sit:

```bash
$ ll scripts/kubernetes/

total 104
drwxr-xr-x 2 karreg karreg  4096 2019-03-06 15:12 .
drwxr-xr-x 8 karreg karreg  4096 2019-03-06 14:48 ..
-rw-r--r-- 1 karreg karreg  2348 2019-03-05 14:58 project_A.config.yml
-rw-r--r-- 1 karreg karreg  1970 2019-03-05 14:59 project_B.config.yml
-rw-r--r-- 1 karreg karreg  5850 2019-03-05 15:02 project_C.config.yml
-rw-r--r-- 1 karreg karreg   157 2019-03-06 15:13 Readme.md
-rw-r--r-- 1 karreg karreg   101 2019-03-05 14:55 shared.config.yml
```

The `shared.config.yml` file contains the shared configuration. For kubernetes, it includes the current context, but you can add any other shared configuration, present or future.

The `project_X.config.yml` files contain the project specific configuration. They can contain one or multiple context definitions.

Once the file are created, you can reference them in the `KUBECONFIG` environement variable.

!!! warning
    Always reference the shared file as first file, since Kubernetes use the first file to store the current context.

```bash
export KUBECONFIG=~/scripts/kubernetes/shared.config.yml:~/scripts/kubernetes/project_A.config.yml:~/scripts/kubernetes/project_B.config.yml:~/scripts/kubernetes/project_C.config.yml
```

You can insert this line in your `~/.bashrc` file, or in a file somewhere in the scripts folder tree (for example `~/scripts/kubernetes/environment.bashrc`), and load it in your `~/.bashrc`. I prefer the second option, since I have the `~/scripts` folder version controlled in a git repository, allowing me to:

* go back in time in case of configuration mistake
* keep my configuration even if my computer is dead
* synchronize this configuration over multiple computers, if needed

To see how to load a file in the `~/.bashrc` file, see the [Versioned bash configuration](versioned_bash_configuration.md) article about organizing bash configuration.
