---
authors:
  - karreg
date: 2024-06-17
categories:
  - development
  - software
tags:
  - docker
  - gitlab
  - gitlab-ci
---
# Multi-arch docker image on GitLab-CI

I have [previously](multi-arch_docker_macos.md) presented how we could build multi-arch docker images on MacOS. But this is just the first steps, as _you don't package on your computer_. This is the responsibility of your CI, which can include testing, security, and above everything else, reproducibility.

<!-- more -->

> [!tip]
> I have created a component and a template following the research and writing of this article. You can find them here: [Docker multi-arch build](https://gitlab.com/karreg/cic-docker-multi-arch-build).

## Pick your solution

I have presented docker as building solution in the previous article, but there are other options, such as:

- [podman](https://podman.io/)
- [buildah](https://buildah.io/)
- [kaniko](https://github.com/GoogleContainerTools/kaniko)

I have been using kaniko for a while now for my CI/CD contexts. It has the good sense not to require privileged runners. However, kaniko does not manage cross platform building.

I don't know about podman and buildah, but I guess they have the same issues as docker.

Maybe they don't require privileged runners either, but I have already faced build issues with podman, as each layer _is a full system image_. That's the result of the lack of privileged accesses, leading to no overlay filesystems. That means that an image with 10 layers, with a 3 GB base image, will use _at least_ 30 GB on the disk.

Fortunately, gitlab.com provides shared privileged runners. So we should be fine to build cross-platform images.

## Job sample

I had a _hard_ time making this job working. Litterature exists on Internet, but both GitLab-CI and buildkit/buildx have evolved since, even in the last few months. Solutions that were working at that time are not working anymore.

I won't be able to describe a iterative solution, to present how this job is working. So, instead, here is the result of my findings.

You will need a few things in your job:

- a `docker:dind` service to provide docker daemon
- a `docker:cli` base image to provide docker command line
- a `qemu-user-static` image to provide qemu

The job will:

1. Start qemu
2. Login to the registry
3. Create and use a docker context
4. Create and use a docker builder
5. Build and push the multi-platform image

Here is the snippet:

```yaml
image:
	name: docker:cli

services:
	- docker:dind

variables:
	DOCKER_TLS_CERTDIR: "/certs"
	PLATFORMS: "linux/amd64,linux/arm64"

before_script:
	# Start qemu
	- docker run --rm --privileged multiarch/qemu-user-static --reset -p yes

	# Create build context
	- docker context create ma-context
	- docker --context ma-context buildx create --name ma-builder --driver docker-container
	- docker buildx use ma-builder
	- docker buildx inspect --bootstrap

	# Login to the registry
	- echo "$CI_REGISTRY_PASSWORD" | docker login -u $CI_REGISTRY_USER --password-stdin

script:
	# Build and push image
	- docker buildx build --platform ${PLATFORMS} --push -t "${CI_REGISTRY_IMAGE}:${TAG}" .
```

Aaaaaaand it's done.

## What if I need custom CA certificates?

If you use self signed certificates or a custom certificate authority, you will need to pass the certificates to the buildkit container **AND** the DIND container.

Let's say your have the certificate(s) in the `CA_CERTS` environment variable.

You can inject the certificates this way in the DIND container:

```yaml
	services:
		- name: docker:dind
		  command:
			- /bin/sh
			- -c
			- |
			  [ -n "$CA_CERTS" ] && \
			  echo "$CA_CERTS" > /usr/local/share/ca-certificates/custom_ca_certs.crt && \
			  update-ca-certificates
			  dockerd-entrypoint.sh
```

And you can inject the certificate in the job and buildkit containers with this script step:

```yaml
	# Prepare the CA certificates 
	- |
	  [ -n "$CA_CERTS" ] && \
	  echo "$CA_CERTS" > /usr/local/share/ca-certificates/custom_ca_certs.crt && \
	  update-ca-certificates && \
	  export BK_OPTS="--config /tmp/buildkitd.toml" && \
	  cat > /tmp/buildkitd.toml <<EOTF
	  [registry."$REGISTRY"]
		ca=["/usr/local/share/ca-certificates/custom_ca_certs.crt"]
	  EOTF
```

The full job becomes:

```yaml
image:
	name: docker:cli

	services:
		- name: docker:dind
		  command:
			- /bin/sh
			- -c
			- |
			  [ -n "$CA_CERTS" ] && \
			  echo "$CA_CERTS" > /usr/local/share/ca-certificates/custom_ca_certs.crt && \
			  update-ca-certificates
			  dockerd-entrypoint.sh

variables:
	DOCKER_TLS_CERTDIR: "/certs"
	PLATFORMS: "linux/amd64,linux/arm64"

before_script:
	# Prepare the CA certificates
	- |
	  [ -n "$CA_CERTS" ] && \
	  echo "$CA_CERTS" > /usr/local/share/ca-certificates/custom_ca_certs.crt && \
	  update-ca-certificates && \
	  export BK_OPTS="--config /tmp/buildkitd.toml" && \
	  cat > /tmp/buildkitd.toml <<EOTF
	  [registry."$REGISTRY"]
		ca=["/usr/local/share/ca-certificates/custom_ca_certs.crt"]
	  EOTF

	# Start qemu
	- docker run --rm --privileged multiarch/qemu-user-static --reset -p yes

	# Create build context
	- docker context create ma-context
	- docker --context ma-context buildx create --name ma-builder --driver docker-container
	- docker buildx use ma-builder
	- docker buildx inspect --bootstrap

	# Login to the registry
	- echo "$CI_REGISTRY_PASSWORD" | docker login -u $CI_REGISTRY_USER --password-stdin

script:
	# Build and push image
	- docker buildx build --platform ${PLATFORMS} --push -t "${CI_REGISTRY_IMAGE}:${TAG}" .
```

> [!tip]
> If you missed the information, you can also use the GitLab-CI component and template I have written here: [Docker multi-arch build](https://gitlab.com/karreg/cic-docker-multi-arch-build).

