---
authors: [ karreg ]
date: 2019-03-06
categories:
- system
- linux
tags:
- versioned_devenv
- bash
---

# Versioned bash configuration

Defining aliases, functions, or just plain scripts, is part of the job when you work a lot in _bash_. I use a file organization to keep my stuff clean, logical and searchable.

<!-- more -->

Of course this is not only usable for _bash_, you can use it for _fish_, or _zsh_, or even multiple environment. On my setup, I use _zsh_ and _bash_, and every function are defined in both environment. Only the file extension changes : `.zsh` for _zsh_ related stuff, and `.bashrc` for _bash_ stuff... I once used fish too, but I quit it, because of a lot of incompatibilities with bash, but that's another story.

So here is my setup. I have a `~/scripts` folder, with a `bashrc.d` sub-folder. In this sub-folder sit my aliases and function files:

```bash
$ ll ~/scripts/bashrc.d/

total 28
drwxr-xr-x 2 karreg karreg 4096 2019-03-05 11:07 .
drwxr-xr-x 8 karreg karreg 4096 2019-03-06 14:48 ..
-rw-r--r-- 1 karreg karreg  280 2019-03-06 15:39 environment.bashrc
-rw-r--r-- 1 karreg karreg  282 2019-03-06 15:18 environment.zsh
-rw-r--r-- 1 karreg karreg   37 2018-10-22 11:40 kubernetes.alias
-rw-r--r-- 1 karreg karreg   86 2018-10-22 11:40 kubernetes.function.bashrc
-rw-r--r-- 1 karreg karreg   86 2019-03-05 11:05 kubernetes.function.zsh
```

As you can see, there's an alias file for alias definition, compatible for both zsh and bash, and a function file for each environment. Files are split per technology or whatever you want. The `environment.bashrc` file is here to setup global environment variables, like `KUBECONFIG` (see [Versioned kubernetes configuration](versioned_k8s_configuration.md), but this environment variable could also be defined in the `kubernetes.function.bashrc`, or maybe a `kubernetes.environment.bashrc` file to keep things clean and separated.

My aliases are just plain aliases, or redirect to function defined in the other files so aliases are defined only once, but available in any environment.

Here is a small example for kubernetes context switch:

```bash
$ cat kubernetes.alias

alias kctx='func_kubectl_context'
```

```bash
$ cat kubernetes.function.bashrc

func_kubectl_context() {
  if [ "$#" -eq 0 ]
  then
    kubectl config get-contexts
  else
    kubectl config use-context $1
  fi
}
```

Now, you can load all these files in your `~/.bashrc` file:

```bash
# INCLUDE_LXSC
for file in ~/scripts/bashrc.d/*.bashrc;
do
  source "$file"
done

for file in ~/scripts/bashrc.d/*.alias;
do
  source "$file"
done
```

Now any file I create in the `~/scripts/bashrc.d` folder will be automatically loaded in my environment.

Another benefit is that I have my `~/script` folder in a git respository, so I can:

* go back in time in case of configuration mistake
* keep my configuration even if my computer is dead
* synchronize this configuration over multiple computers, if needed
