---
authors: [ karreg ]
date: 2019-07-24
categories:
- software
- keepass
tags:
- keepass
---

# Vanilla KeePass

Nowadays, everybody is aware that (good) passwords (strategies) are important, but knowledge on *why* is not as widespread, and we can still see more or less complex passwords, used on several websites with different accounts, and a handful of password for all services, just to be sure they are not forgotten.

<!-- more -->

However, solutions exist. They are not perfect, but no solution is perfect and they are still a great improvement.

One can wonder if this is a good idea to store password on a private service, like `Dashlane`, `1Password` or `LastPass`. But this is still far, far better than using the same password everywhere. So, do not hesitate to choose one of these services if it brings you some important feature.

I won't talk about these services. I will talk about Keepass. In a mitigated context, between usability and security. Don't expect the most secure solution (spoiler: even the most secure solution is not absolutely secure, but be sure that using it will be a living hell). You may still want something more secure. If that is the case, it's up to you. But if you are into security, I expect you to have been using a password manager for at least a decade, and you probably won't learn anything here.

I won't talk about Keepass alternatives either, be them KeepassX, KeepassXC or KeeWeb... They are undoubtely useful, but, to me, they are still inferior to Keepass. Even on Linux where Keepass is not native, requires a *huge* dependency, and comes with lags so sometimes auto-type does not work.

> If I get enough feedback on this subject, I may deep dive in it and explain why I prefer Keepass, and you will have all the time you want to convince me I'm wrong (I would be glad to have the same features in KeepassXC!).

We are almost done with this introduction. We can finish it with a handful of good practices that improves security of your personal data:

- Don't use the same password multiple times. Never, ever. If one of the websites is corrupted, and has stored the users' passwords in an uncrypted database, expect your login and password to be public. Anyone can try to connect to any other system (webmails, socials networks...) with the same login and password... That is almost the worst that can happen.
- Use strong passwords. Mix letter, numbers and specials characters. And a lot of them, not just 6 or 8. The longer the password, the stronger it is. Another strategy is to use sentences. Sentences are easier to remember, quick to type, and really difficult to force. If you can add a few numbers and specials characters in it, it is even better. Here is the best place for this famous [XKCD](https://www.xkcd.com) comic:
  
  ![XKCD](https://imgs.xkcd.com/comics/password_strength.png)
- Use two-factor authentication whenever you can. Especially on critical services: 
  - Email. Because you have far too much personal and sensible data. Anyone that get access to your email account basically has access to any of your accounts, and can also block you from accessing them. Forever, since you won't be able to restore your password. You get it, it requires access to your email... **This** is the worst that can happen to you.
  - Social networks. Identity fraud towards your *friends*... But not only. An access to your social network account is a great tool for social engineering. This method is more and more used to get personal data on you, to be able to retrieve your other password by answering questions about you. This is not the most used method though, because it requires involvement. But if it were to happen to you, be sure you are facing someone that is strongly resolved to get **your** personal data. You don't want to know why, you just want to avoid that.
  - Generally speaking, on any site that contains personal and/or sensible data about you.
- Use several identities. At least 2, your real identity with a dedicated email address, for public services, your family, your friends... And a pseudonymous one, with a different email address, for virtual identities and people you don't personally know. Forums for example.

These few rules are quite easy to follow, and increase your security without being a great deal.

We can now start with the real subject. Thanks for still being there... ;)

<!-- toc -->

## Password database creation

Keepass can be found on [its website](https://keepass.info/index.html).

When you start it, you can create a new database. A database is a file that contains your password entries, protected by encryption and requiring a password to load.

![Master password](vanilla_keepass/vanilla_keepass-01.png)

Choose a very (very) strong password for your database, that only you can remember. It will be the (almost) only password you will need to remember from now on. This is the time where you want to create a password sentence with the name of your ancestors on 3 generations. Good luck to anyone that want to guess such a password, even with social engineering.

But you don't want to forget it either. If you forget it, you won't be able to open the database anymore. No workaround. Keepass will propose you to print the master password later. But you can still write it down on a paper that you will securely store somewhere (because [paper is still more secure than using the same password everywhere](https://www.vox.com/2014/4/16/5614258/the-best-defense-against-hackers-writer-your-passwords-down-on-paper)). You can also write down hints that help you remember your password.
  
On the next form, in the `Security` tab, select the `Argon2` derivation function.

The other important value is the `Iterations` value. This is the number of iterations where the file is encrypted over itself. Imagine your database being a paper in a Russian doll. The iteration count is the number of dolls that you have to open before getting to the paper. It makes brute force more complicated. The higher value, the more complicated.

The `1 Second Delay` button will setup the value for you, depending on the power of your computer. Basically, it will ensure the password file is opened within 1 second. If you open the same database on another computer it could be faster or slower, but nothing really noticeable. However, it could be far more longer on your smartphone if you want to use it there too, depending on the smartphone specifications, and the software used. Keepass2Android has now (starting version 1.07b, still in beta in early september 2019) a good implementation, and keeping desktop values could be good enough. However, you can reduce the iteration count if needed.
  
![Database settings](vanilla_keepass/vanilla_keepass-02.png)

## Add an existing password

To add an existing password, you have to create a new entry.

![Add entry](vanilla_keepass/vanilla_keepass-03.png)

The most important fields are:

- Title : A title that helps finding the entry. It is also used by auto-type, we will see this later.
- User name : The username/login
- Password : The password. There is already a value, but you just have to overwrite it with the existing value
- Quality : Estimates the password quality. It could be better here for example.
- URL : The website URL, if meaningful. Is also used by auto-type.

There are a lot of other fields. Some will be explained later.
  
## Create a new password

The way to create a new password is the same. The password is generated for you when the entry is created, you just have to use it when registering on the website.

The interesting option here is the password generator. It is available from the menu right of the password.

![Password generator](vanilla_keepass/vanilla_keepass-04.png)

You can use the options you want. The ones presented here give a good password quality. But if you encounter a website with specific password constraints (character count limit, or digits only...) this is where you can customize the password generation.

The worst are websites that don't tell you why a password is not valid, they don't even have clear rules on password... You can only try until it fits... :(

## Auto-type

Auto-type is what makes Keepass so powerful. The default shortcut for this feature is `CTRL+ALT+A`. When you are in a login form, place the cursor in the login field, and press `CTRL+ALT+A` and let the magic do the work for you. If it does not, then you need to improve your entry.

What is happening when using auto-type? Keepass will get the name of the window the cursor is in. It will look for an entry matching this criteria: `Is the entry Title contained in the window's name?`. If yes, the entry will be selected for auto-type... If multiple entries match, Keepass will ask you to select the correct entry between matching entries.

> The URL field is not used by auto-type. It can be used by web browser plugin that connect to Keepass to input passwords for you. I am not discussing this use case, since it is not needed for me. A good auto-type will work on a web browser too, without the need of another plugin...

The window's name can be the application name in case of desktop application. For websites, the window's name is the name of the web page. In general it contains the website's name in it. However, it is not necessarily the case.

What if the title can't be used? This is where auto-type configuration comes in. Open the associated entry and check the `Auto-Type` tab.

![Autotype configuration](vanilla_keepass/vanilla_keepass-05.png)

Click on `Add` and select the window where you want your password to be typed.

![Autotype configuration](vanilla_keepass/vanilla_keepass-06.png)

> For the record, in this particular case, Having an entry title named `GitHub` will work. But let's assume it is not, for any reason...

If you are targetting a desktop application, selecting it is enough, you validate, and *voilà*.

In the case of a website, what the window name is telling us is that I'm trying to connect to GitHub in the [Brave](https://brave.com) browser. Should it be Chrome, Chromium, Firefox or Edge, the browser name would appear after the website page in the same way : `<Website page name> - <Browser name>`. You just have to use wildcard (`*`) instead of browser name to make it work on **any** browser... That is precisely why Auto-type is mort powerful than a browser extension. You can switch browser and still have auto-type to working.

In this case, the entry would be `Sign in to GitHub · GitHub*`.

> Note that wildcards are not working at the beginning of the target window name...

![Autotype window name](vanilla_keepass/vanilla_keepass-07.png)

We will see later what is the keystroke sequence, and what can be done with it.

## References

Keepass lets you use references on logins and/or passwords (and basically on anything).

References are, as expected, replacing values by reference to another value. Let's say you use the same email **everywhere**. Instead of typing the same email in every entry, you just need to create an entry and reference it everywhere else. Same goes for passwords.

![Reference entry](vanilla_keepass/vanilla_keepass-08.png)

To reference its fields in another entry, use references.

![Create reference 1](vanilla_keepass/vanilla_keepass-09.png)

![Create reference 2](vanilla_keepass/vanilla_keepass-10.png)

The fields will be fields with references instead of values.

![Reference result](vanilla_keepass/vanilla_keepass-11.png)

To make it clear, I strongly recommend **not** using it in a personal password database. The reason behind is that if you change the email entry, every entries will be updated at the same time, even though all the associated accounts aren't updated. You basically lose your login by doing so. Of course I strongly recommend **not** referencing passwords too, because **each service should have a difference password**.

But there is a case where this is extremely useful: corporate environments, where all your accounts are linked to a single entry in a corporate directory. When your email is changed, it is changed everywhere at the same time. When you password is changed, it is changed everywhere at the same time. In this case, use references.

But of course, this is a matter of choice. There is another way to achieve the same result. You get it: Auto-type... Instead of creating an entry for each service using the same corporate directory entry, you create a single corporate directory entry, with an auto-type configuration for each service using it... It's up to you to choose one or the other, or a mix for any reason you could have.

## Password analysis

There a few great tools in Keepass to analyze your passwords...

![Password analysis menu](vanilla_keepass/vanilla_keepass-12.png)

### Duplicate passwords

Duplicate passwords will show you the entries that share the same passwords. Perfect when you setup your database and want to improve you passwords by iterating. It will also show references.

![Duplicate passwords](vanilla_keepass/vanilla_keepass-13.png)

### Similar passwords

This tool will show similar passwords where only a few characters are changed. This is a bad practice that should be avoided. Known passwords are not only plainly tested on websites. They are also mutated to test similar passwords...  A similar password if roughly more secure than the same password. The value you want to check if the similarity percentage. The more short password, the more similar to any other password... Here, similarity down to 6% can be ignored. But 86% similarity **is really bad**.

![Similar passwords](vanilla_keepass/vanilla_keepass-14.png)

The cluster version is a bit more readable, and display groups of password that share the similarity. It is basically the same data, but with a different view.

![Similar password clusters](vanilla_keepass/vanilla_keepass-15.png)

### Password quality

This tool display in a single view the quality of all your password, so you can work on the more critical first. But work on duplication and similarity first. By doing so, you will fix bad password quality at the same time, and you won't have as much entries to handle here.

![Password quality](vanilla_keepass/vanilla_keepass-16.png)

## Auto-type sequences

Sometimes, badly coded websites don't use the standard auto-type sequence. This sequence is `type login -> TAB -> type password -> ENTER` and is compatible with more than 95% of the known websites (this value has no backing, it could be 90% or 99% it's not really the point). But sometimes, web developers find it funny to display only the login field, then type `ENTER`, then display the password field... It is not only *funny*, it is also slower, and bad for security, since password manager badly handle these cases... But who cares about security huh?

The solution here is to change the auto-type sequence. You can either change it for the whole entry, or by auto-type entry.

![Update autotype sequence](vanilla_keepass/vanilla_keepass-17.png)

There is nothing complicated here... You try to find the correct sequence manually on the website, with the correct combination or `TAB` and `ENTER`, and you copy this sequence in the keystroke sequence field. Here, the sequence contains 3 `TAB` between the login and password.

![Updated autotype sequence](vanilla_keepass/vanilla_keepass-18.png)

In the case of multiple pages you can use something like this:

`{USERNAME}{ENTER}{DELAY 2000}{PASSWORD}{ENTER}`

The delay is here to handle the time needed to load the password form... It could be enough, but sometimes it could not be enough. In this case, auto-type will randomly fail... Use the best value you can, without needing to wait for 30 seconds to login... Ha! How funny! Well done web developers... :D

## Keyboard macros

This is not what Keepass is for, but since it emulates a keyboard, why not using is for keyboard macros? For example, I have a macro for connecting VPN with two-factor authentication. It looks like that... In the VPN main windows:

1. I auto-select the good VPN connection and wait for the password form: `{TAB}{TAB}{ENTER}{DELAY 3000}`
2. I open the soft token app which name starts with *strong*: `{WIN}STRONG{DELAY 200}{ENTER}`
3. I input the PIN to get my token, copy it and close the app: `{PASSWORD}{ENTER}{TAB}{TAB}^A^C%{F4}`
4. Back in the VPN pass form, I copy the value: `^V{ENTER}`

In a single sequence, the auto-type value becomes:

`{TAB}{TAB}{ENTER}{DELAY 3000}{WIN}STRONG{DELAY 200}{ENTER}{PASSWORD}{ENTER}{TAB}{TAB}^A^C%{F4}^V{ENTER}`

The only limits are your imagination, and the target applications. If your app if not intended to be controlled by keyboard, you're doomed...

## Conclusion

That's it for basic to advanced usage with Keepass built-in features. Next article will be about plugins...
