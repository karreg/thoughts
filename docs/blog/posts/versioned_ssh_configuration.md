---
authors: [ karreg ]
date: 2019-03-06
categories:
  - system
  - linux
tags:
  - versioned_devenv
  - ssh
---

# Versioned ssh configuration

If you use ssh on a lot of different servers, you probably use the famous `~/.ssh/config` file.

But if you really have a lot of servers to manage, this file can quickly grow, and become unreadable.

<!-- more -->

Fortunately, ssh now allow the inclusion of external files in its config file, and we can use that to split the configuration in multiple, more readable, files. Here's my setup, but it can easily be done in another way...

I have a `~/scripts` directory, containing a ssh subdirectory where my files sit:

```bash
$ ll ~/scripts/ssh/

total 40
drwxr-xr-x 2 karreg karreg 4096 2019-03-06 15:10 .
drwxr-xr-x 8 karreg karreg 4096 2019-03-06 14:48 ..
-rw-r--r-- 1 karreg karreg  422 2019-03-06 15:09 project_A.config
-rw-r--r-- 1 karreg karreg  501 2019-03-06 15:07 project_B.config
-rw-r--r-- 1 karreg karreg   96 2019-03-06 14:50 project_C.config
-rw-r--r-- 1 karreg karreg  104 2019-03-06 15:11 Readme.md
-rw-r--r-- 1 karreg karreg  891 2019-03-06 15:06 shared.config
```

The shared.config file contains shared configuration, like ssh proxies, tunnels or hop servers.

The `project_X.config` files contain project specific access, that can reference hop servers in the `shared.config` file.

In the end, my `~/.ssh/config` only contains include directives for these files:

```bash
$ cat ~/.ssh/config

Include ~/scripts/ssh/shared.config
Include ~/scripts/ssh/project_A.config
Include ~/scripts/ssh/project_B.config
Include ~/scripts/ssh/project_C.config
```

This configuration becomes more readable, but that's not enough. Now I can add the `~/scripts` in a git repository and...

* go back in time in case of configuration mistake
* keep my configuration even if my computer is dead
* synchronize this configuration over multiple computers, if needed
