---
authors: [ karreg ]
date: 2021-05-03
categories:
- release_management
tags:
- release_management
- checklist
- gitlab
---

# How to manage releases with GitLab

Releases are more than just deployment in general.

Continuous deployment is great, but it's really, really hard to have a suitable architecture. It requires to be a critical component in your business to make it worth the invest. Because micro-services, API versioning and data backward compatibility is a pain...

<!-- more -->

For smaller projects, it often means that it would be more expensive without any added value with continuous deployment. But you can still use continuous delivery, meaning your deployment is automated, but you still have some manual steps. The most basic manual step being starting the automated deployment.

This article is about how we can use GitLab to manage what is not automated.

## A bit of history

In 2018, I was suddenly assigned to manage corporate instances of GitLab. At that time, the deployment process was manual with some steps automated in an undocumented script. As GitLab is a critical part in the development toolchain for thousands of users, I was not at ease with that. Especially since I have put in place multiple automated deployment systems in the years before.

Before thinking about automating the deployment process, it was all about documenting it first, with a full, manual, checklist. A checklist that would be replicable for each release. The idea was to then automate it bits by bits, and update the checklist to keep it as small as possible.

But before looking for another tool to do that, I have tried with the tools already available. And since I'm a big supporter of keeping everything under version control, as close as possible to the source code, GitLab was the main target. Especially issues.

Why not CI/CD? Well, the first answer is that, at that time, GitLab-CI was not enabled. I then enabled it a few months later. But the other reason was: some steps are easy to automate (deployment), some steps are a bit harder to automate (declaring a maintenance period in the monitoring tool), and some are really too much expensive to automate to make it worth, like publishing an alert on the corporate communication portal

The checklist would be a text file, easy to maintain and easier to use. Here comes the GitLab flavored markdown, and [this](https://docs.gitlab.com/ee/user/markdown.html#task-lists): Checkboxes in Markdown.

The last missing piece was the [issue template](https://docs.gitlab.com/ee/user/project/description_templates.html) feature.

## Using GitLab as a Release procedure tool

### Added values

Soon enough, I found added values to have the release procedure as GitLab issues.

The first one is that the procedure is linked to a commit, or even better, a tag. With automation improvement, the tag would be the link between the automated deployment (pipeline) and the manual actions (issue).

The second one is that the procedure is versioned. Each change can be reviewed by a team member, and we can go back in time if needed.

The third one is to be able to comment the release in the issue _during_ the release process, allowing us to come back to it later to improve the release process. Of course, comments are multi-users, so it's easier to interact with other actors of the release.

The last one is to be able to label the release. Was it successful or not? Label. What was the last failed release and why? Label filter.

Let's see how we can use GitLab for all that.

### Writing a (dynamic) checklist

First thing first, writing a checklist in GitLab is really easy.

```markdown
Here is my checklist:

* [x] Item 1
* [ ] Item 2
* [ ] Item 3
```

In this case, the item 1 is checked, where the items 2 and 3 are not. Of course, in your template, nothing is checked. here is the result in GitLab:

![_GitLab issue with checklist_](release_management_with_gitlab/gitlab-rm-01.png)

The checkboxes are dynamic, you can click to check/uncheck them. And everything is traced in the issue.

![_Greg, WTF is wrong with you?!_](release_management_with_gitlab/gitlab-rm-02.png)

Comments are available to write down the difficulties met, unexpected behaviors, questions to someone else...

![_Greg, WTF is wrong with you⁽²⁾?!_](release_management_with_gitlab/gitlab-rm-03.png)

And this is enough to write a release procedure.

But if your steps require quite long paragraphs, your checklist may become less readable, which could in turn lead to missed steps.

![_Small checklist, yet already hardly readable_](release_management_with_gitlab/gitlab-rm-04.png)

Fixing this part was not easy, since it implies a few undocumented, unpredictable behaviors in the Markdown engine.

The first option is to use [collapsable blocks](https://about.gitlab.com/handbook/markdown-guide/#collapse). It will keep your checklist really clean...

![_Collapsing is great but..._](release_management_with_gitlab/gitlab-rm-05.png)

... but it will break some markdown blocks, for example here the code block. No solution. It can still be the preferred solution if you don't need broken markdown in your collapsable blocks.

The second option, if the previous one is not working for you, is to use [blockquotes blocks](https://about.gitlab.com/handbook/markdown-guide/#blockquotes) instead of collapsable blocks. The checklist won't be as clean as with collapsable blocks, but it will still be far more readable than a basic list. Plus, code blocks are compatible with blockquotes with a few tricks.

![_Blockquotes are better than nothing_](release_management_with_gitlab/gitlab-rm-06.png)

Here is the list of the needed tricks to make this work:

* Blockquote character (`>`) must be preceded by less than 6 blank spaces
* Line breaks must be forced with 2 blank spaces at the end of the line

Here is the code used for the above sample:

```markdown
## Release

* [ ] Announce the release.  
  > You have to go on the [corporate portal](https://link-to-corp-portal/folder) and create a new announcement with the following template:  
  > ```
  > A new version of your favorite tool will be deployed on DATE at TIME. A downtime of MINUTES is expected.  
  > Here is a full Release Note:  
  > RELEASE NOTE
  > ```
* [ ] Add the maintenance period in Zabbix
  > * Go on the [Zabbix monitoring portal](https://link-to-zabbix).  
  > * In the service section, look for the tool group.  
  > * Go to Periods -> Add -> One Time Only and set the begin and end time  
  > * Click on Add before clicking on Update
* [ ] Announce the release in Mattermost  
  > Go to Mattermost, on the #tool-annoucement channel and write a message with a link to the announcement  
  > ```  
  > Hello @here, a new version of your favorite tool with be deployed on DATE at TIME.  
  > A downtime of MINUTES is expected.
  > More information on the announcement on the corporate portal (with LINK)
  > ```
```

### Adding a TOC

You may have noticed the presence of a Title 2 block `## Release` before the checklist.

To be fully usable, the template should include the pre-release steps (communication, release validation...), the release steps (deployment), the post-release steps (controls, communication), the rollback steps, and the post-rollback steps (controls communication).

Using titles is the minimum to organize all these steps. But it can lead to a big checklist.

Also, pre-release steps can be executed days before the release steps.

Adding a table of content will make the procedure easier to use. you just need to:

* Use a title for each part of your release template
* Generate a list with links to the expected HTML anchor that will be generated

For example, if you have the following sections in your template:

```markdown
## Template instructions

How to use the template

## Release Description

Description of the release (date, time, version, release note, link to git tag...)

## Release

Release steps

## Rollback

Rollback steps
```

You can use the following table of content block at the beginning of your template. The easiest way to get the associated anchors is to create an issue, and copy/paste the generated anchors...

```markdown
* [TEMPLATE INSTRUCTIONS](#template-instructions)
* [RELEASE DESCRIPTION](#release-description)
* [RELEASE](#release)
* [ROLLBACK](#rollback)
```

![_Long Release procedures are easier to use with a TOC_](release_management_with_gitlab/gitlab-rm-07.png)

### Using labels to organize releases

You can use issue label system to organize your release. The simpliest organization would use 3 labels:

* `Release` label will separate release issues from other issues
* `Released` label will mark a successful release
* `Rollbacked` label will mark a failed release

The first label can be automatically assigned from the template itself by using [GitLab quick actions](https://docs.gitlab.com/ee/user/project/quick_actions.html#issues-merge-requests-and-epics).

Simply add this code anywhere in your template:

```markdown
/label ~Release
```

The best way to apply other labels is to include the command in the release procedure. And at the same time you can close the issue with another quick action.

```markdown
## Post-Release

* Check the logs
* Check the login page
* Check Zabbix
* Update the issue with a comment
      > ```text
      > This is a success /shrug
      > /label ~Released
      > /close
      > ```
```

Once your releases are labelled, you can filter your issues on any label, depending on what you are looking for...

![_Filtered issues_](release_management_with_gitlab/gitlab-rm-08.png)

### Using GitLab issues template to (easily) create new releases

Once your issue template has been created, you just need to create a file in your git repository, in `.gitlab/issue_templates/release.md`. Of course you can have multiple templates, you will then be able to select the one you need when creating a new issue.

![_Select your template when creating a new issue_](release_management_with_gitlab/gitlab-rm-09.png)

Select the template you need, follow the instructions, and your release procedure is ready for use.

![_Ready to release_](release_management_with_gitlab/gitlab-rm-10.png)

Even the labels and due dates are set.

![_Labels and due date are set_](release_management_with_gitlab/gitlab-rm-11.png)

## More ideas

The same strategy can be used for other use cases, like:

* Standard but manual support
* Onboarding / Offboarding a team member checklist
* Team checklist for vacation

## Improvements

Some improvements could be made, but I think it's too far away from the initial use case to see GitLab work on it.

The first improvement could be on collapsable blocks, to allow using other blocks inside, to have even more clear release procedures.

The second improvement would be variable management. Instead of having to update the target version at different places in the procedure, you could define a variable at the beginning, and use it everywhere it's needed. Something like:

```markdown
set(TARGET_VERSION=1.2.3)
set(RELEASE_DATE=2021-05-06)

...

Target version: $TARGET_VERSION
/due $RELEASE_DATE
...

* [ ] Create a tag for this release named Release-$RELEASE_DATE-$TARGET_VERSION

...
```

## Feedback

Today, I'm far away from the initial manual deployment. I use Terraform and Ansible to fully automate the infrastructure creation and deploy GitLab in a High Availability architecture. But we are still using release templates for all the manuals steps, including communication and manual controls...

The strategy has spread to other team members on other tools.

I hope it will help you too...
