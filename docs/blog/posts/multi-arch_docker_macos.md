---
authors:
  - karreg
date: 2024-06-04
categories:
  - development
  - software
tags:
  - docker
  - macos
---
# Multi-arch docker image on MacOS

I have been looking at MacOS as a development computer, and fast enough I have been facing issues with Docker, leading to building multi-arch docker images, leading to further issues.

Here is my story.

<!-- more -->

## Colima: docker for MacOS

As for Windows, docker does not run natively on MacOS. As for Windows, this is because docker requires linux kernel features (cgroups). So, as for Windows, docker on MacOS requires a virtual machine with a linux kernel to execute Docker containers.

The simplest way to do it is to use Docker Desktop.

But if you can't afford using Docker Desktop, [Colima](https://github.com/abiosoft/colima) is your way to go.

Installing Colima is as easy as:

```bash
$ brew install colima docker docker-buildx
```

`colima` is the backend that will start a virtual machine and expose the docker socket to your host, `docker` is only used only for the client-side CLI, and `docker-buildx` is used to install the buildx plugin.

You will see a warning message telling you to manually add the plugin configuration in your docker configuration. So open `~/.docker/config.json` and add the following configuration:

```json
	"cliPluginsExtraDirs": [
		"/opt/homebrew/lib/docker/cli-plugins"
	]
```

Colima must be started by hand. It will pop a default virtual machine, but this virtual machine is way too small for standard docker usage, especially with amd64 emulation. So let's customize this VM.

You can configure multiple virtual machine with the `--profile` option, but for now we will stick to the default VM (equivalent to `--profile default`).

```bash
$ colima start --edit
```

Adapt the default configuration to your usage. On my Mac M2 Pro I have set up the following:

```
# Number of CPUs to be allocated to the virtual machine.
# Default: 2
cpu: 4

# Size of the disk in GiB to be allocated to the virtual machine.
# NOTE: changing this has no effect after the virtual machine has been created.
# Default: 60
disk: 120

# Size of the memory in GiB to be allocated to the virtual machine.
# Default: 2
memory: 8
```

A bit further, I have configured some other features.

The ssh agent forward is needed for git over ssh in devcontainers, or if you need ssh access from them:

```
# Forward the host's SSH agent to the virtual machine.
# Default: false
forwardAgent: true
```

The VM type set to Rosetta _should_ offer improved performances over `qemu` for x86_64 emulation:

```
# Virtual Machine type (qemu, vz)
# NOTE: this is macOS 13 only. For Linux and macOS <13.0, qemu is always used.
#
# vz is macOS virtualization framework and requires macOS 13
#
# Default: qemu
vmType: vz

# Utilise rosetta for amd64 emulation (requires m1 mac and vmType `vz`)
# Default: false
rosetta: true

# Volume mount driver for the virtual machine (virtiofs, 9p, sshfs).
#
# virtiofs is limited to macOS and vmType `vz`. It is the fastest of the options.
#
# 9p is the recommended and the most stable option for vmType `qemu`.
#
# sshfs is faster than 9p but the least reliable of the options (when there are lots
# of concurrent reads or writes).
#
# Default: virtiofs (for vz), sshfs (for qemu)
mountType: virtiofs
```

After saving the configuration, the VM will pop.

It may take a while, but after that, you should be able to request your docker daemon:

```bash
$ docker container ls
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

You can also check that `docker buildx` answers correctly:

```bash
$ docker buildx version
github.com/docker/buildx v0.14.1 Homebrew
```

Finally, you can verify that colima is executed with the intended options:

```bash
$ colima status
INFO[0000] colima is running using macOS Virtualization.Framework
INFO[0000] arch: aarch64
INFO[0000] runtime: docker
INFO[0000] mountType: virtiofs
INFO[0000] socket: unix:///Users/meuporg/.colima/default/docker.sock
```

At this point, you should have a working docker setup, fully compatible with devcontainers. You can also build images with the good old `docker build` command or `docker buildx build`.

Let's try with the following Dockerfile:

```Dockerfile
FROM alpine:latest
RUN echo noop
```

With `docker build`:

```bash
$ docker build .
[+] Building 2.1s (6/6) FINISHED                                                                                                                                             docker:colima
 => [internal] load build definition from Dockerfile                                                                                                                                  0.0s
 => => transferring dockerfile: 70B                                                                                                                                                   0.0s
 => [internal] load metadata for docker.io/library/alpine:latest                                                                                                                      1.4s
 => [internal] load .dockerignore                                                                                                                                                     0.0s
 => => transferring context: 2B                                                                                                                                                       0.0s
 => [1/2] FROM docker.io/library/alpine:latest@sha256:77726ef6b57ddf65bb551896826ec38bc3e53f75cdde31354fbffb4f25238ebd                                                                0.5s
 => => resolve docker.io/library/alpine:latest@sha256:77726ef6b57ddf65bb551896826ec38bc3e53f75cdde31354fbffb4f25238ebd                                                                0.0s
 => => sha256:77726ef6b57ddf65bb551896826ec38bc3e53f75cdde31354fbffb4f25238ebd 1.85kB / 1.85kB                                                                                        0.0s
 => => sha256:1c3b93ed450e26eac89b471d6d140e2f99488f489739b8b8ea5e8202dd086f82 528B / 528B                                                                                            0.0s
 => => sha256:442043f030d33ff05cf2b6dcb64a3948ce38f2663d146558281a828d78eae5fd 1.49kB / 1.49kB                                                                                        0.0s
 => => sha256:94747bd812346354fd5042870b6e44e5406880a4e6b5736a9cf9c753110df11b 4.09MB / 4.09MB                                                                                        0.4s
 => => extracting sha256:94747bd812346354fd5042870b6e44e5406880a4e6b5736a9cf9c753110df11b                                                                                             0.1s
 => [2/2] RUN echo noop                                                                                                                                                               0.1s
 => exporting to image                                                                                                                                                                0.0s
 => => exporting layers                                                                                                                                                               0.0s
 => => writing image sha256:7d576a3490779899e5f32d86d37c0920cf309e8ae555d7ae86a541be78fef031                                                                                          0.0s
```

And with `docker buildx build .`:

```bash
$ docker buildx build .
[+] Building 0.4s (6/6) FINISHED                                                                                                                                             docker:colima
 => [internal] load build definition from Dockerfile                                                                                                                                  0.0s
 => => transferring dockerfile: 70B                                                                                                                                                   0.0s
 => [internal] load metadata for docker.io/library/alpine:latest                                                                                                                      0.4s
 => [internal] load .dockerignore                                                                                                                                                     0.0s
 => => transferring context: 2B                                                                                                                                                       0.0s
 => [1/2] FROM docker.io/library/alpine:latest@sha256:77726ef6b57ddf65bb551896826ec38bc3e53f75cdde31354fbffb4f25238ebd                                                                0.0s
 => CACHED [2/2] RUN echo noop                                                                                                                                                        0.0s
 => exporting to image                                                                                                                                                                0.0s
 => => exporting layers                                                                                                                                                               0.0s
 => => writing image sha256:7d576a3490779899e5f32d86d37c0920cf309e8ae555d7ae86a541be78fef031                                                                                          0.0s
```

You may have seen that, though the output is different, the process is the same. In fact in the second execution, everything is cached. This is because now `docker build` has become an alias for `docker buildx build`.

But it won't work for multi-arch build yet:

```bash
$ docker buildx build --platform linux/amd64,linux/arm64 .
[+] Building 0.0s (0/0)                                                                                                                                                      docker:colima
ERROR: Multi-platform build is not supported for the docker driver.
Switch to a different driver, or turn on the containerd image store, and try again.
Learn more at https://docs.docker.com/go/build-multi-platform/
```

Now let's have a look at how to build multi-arch images to profit near-native performances.

## Docker buildx and BuildKit

First a bit of context and minimal knowledge.

[`BuildKit`](https://github.com/moby/buildkit) if the new image build toolkit developed by the docker team.

`buildx` is a docker CLI extension that allows using the BuildKit features.

You will often see one or the other name, they just are the two faces of the same coin.

Another concept we will have to handle with BuildKit is the concept of _builder_. A builder is a building context. Multiple builders allow to have multiple contexts with different setup or features.

The main difference between builders is the used driver. There are 4 of them, here is a small description:

- `docker` driver uses directly the local docker daemon.
- `docker-container` provides execution inside a container. It comes with issues, like configuration not shared with the underlying docker daemon, but it is also allowing multi-arch build. This is the driver we will use.
- `kubernetes` driver provides builders in a kubernetes environment.
- `remote` is the less useful driver that provides connection to a remote BuildKit daemon.

The `docker` driver, the default one, does not provide multi-arch build. However, `docker-container` does. To use it, we have to create a builder:

```bash
$ docker buildx create --name ma-builder --driver docker-container
```

Now we can specify the builder to build our multi-arch image:

```bash
	$ docker buildx build --platform linux/amd64,linux/arm64 . --builder=ma-builder
	[+] Building 8.7s (9/9) FINISHED                                                                                                                               docker-container:ma-builder
	 => [internal] booting buildkit                                                                                                                                                       5.8s
	 => => pulling image moby/buildkit:buildx-stable-1                                                                                                                                    5.5s
	 => => creating container buildx_buildkit_ma-builder0                                                                                                                                 0.3s
	 => [internal] load build definition from Dockerfile                                                                                                                                  0.0s
	 => => transferring dockerfile: 70B                                                                                                                                                   0.0s
	 => [linux/arm64 internal] load metadata for docker.io/library/alpine:latest                                                                                                          1.8s
	 => [linux/amd64 internal] load metadata for docker.io/library/alpine:latest                                                                                                          2.2s
	 => [internal] load .dockerignore                                                                                                                                                     0.0s
	 => => transferring context: 2B                                                                                                                                                       0.0s
	 => [linux/arm64 1/2] FROM docker.io/library/alpine:latest@sha256:77726ef6b57ddf65bb551896826ec38bc3e53f75cdde31354fbffb4f25238ebd                                                    0.5s
	 => => resolve docker.io/library/alpine:latest@sha256:77726ef6b57ddf65bb551896826ec38bc3e53f75cdde31354fbffb4f25238ebd                                                                0.0s
	 => => sha256:94747bd812346354fd5042870b6e44e5406880a4e6b5736a9cf9c753110df11b 4.09MB / 4.09MB                                                                                        0.5s
	 => => extracting sha256:94747bd812346354fd5042870b6e44e5406880a4e6b5736a9cf9c753110df11b                                                                                             0.0s
	 => [linux/amd64 1/2] FROM docker.io/library/alpine:latest@sha256:77726ef6b57ddf65bb551896826ec38bc3e53f75cdde31354fbffb4f25238ebd                                                    0.4s
	 => => resolve docker.io/library/alpine:latest@sha256:77726ef6b57ddf65bb551896826ec38bc3e53f75cdde31354fbffb4f25238ebd                                                                0.0s
	 => => sha256:d25f557d7f31bf7acfac935859b5153da41d13c41f2b468d16f729a5b883634f 3.62MB / 3.62MB                                                                                        0.3s
	 => => extracting sha256:d25f557d7f31bf7acfac935859b5153da41d13c41f2b468d16f729a5b883634f                                                                                             0.1s
	 => [linux/amd64 2/2] RUN echo noop                                                                                                                                                   0.1s
	 => [linux/arm64 2/2] RUN echo noop                                                                                                                                                   0.0s
	WARNING: No output specified with docker-container driver. Build result will only remain in the build cache. To push result image into registry use --push or to load image into docker use --load
```

The warning message is telling us that our built image is not available, even locally... It stays in the builder cache. We have to manually publish it.

However, local publication does not work with buildkit and the docker-container driver. We can only publish the result in a registry:

```bash
docker buildx build --platform linux/amd64,linux/arm64 . --builder=ma-builder -t ${REGISTRY}/${REPOSITORY}/${IMAGE}:${TAG} --push
```

And that's it. You can now build and publish multi-arch docker images from your Mac, and profit from the near-native performances.

## Handle platform specificities

You may need to do something specific, based on the target platform. My main usage is downloading a binary from GitHub.

You will need the platform as a variable to do so, this is done this way:

```Dockerfile
# Declare use of build-in variable
ARG TARGETARCH

# Install Terraform
RUN    wget -q https://releases.hashicorp.com/terraform/1.4.6/terraform_1.4.6_linux_${TARGETARCH}.zip -O terraform_binary.zip \
    && unzip terraform_binary.zip -d /usr/local/bin/ \
    && rm terraform_binary.zip
```

In the context of multi-stage build, _you must declare the `TARGETARCH` variable in the stage where you need it_.

You also have access to other variables, depending on your needs, like `TARGETOS`. Here is a simple decomposition:

| PLATFORM      | TARGETARCH | TARGETOS |
| ------------- | ---------- | -------- |
| `linux/arm64` | `arm64`    | `linux`  |
| `linux/amd64` | `amd64`    | `linux`  |

## And what about CI/CD?

I will dedicate [another post](multi-arch_docker_gitlab-ci.md) to this matter, since there's no _good_ solution...
