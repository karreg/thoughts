---
authors: [ karreg ]
date: 2020-11-18
categories:
  - development
  - api
tags:
  - proxy
  - gitlab
  - api
  - access_control
---

# GitLab runners control system

_Update 2021-01-18: this article has been rewritten, in a more detailed series of articles. See [here](gitlab_runner_security_risks.md) to find them._

GitLab is a great piece of a tooling chain, even a full chain if your needs fit. But there's something GitLab is not good at all : business use cases. Of course, there is a Entreprise version, and of course it comes with some _Business oriented_ use cases, but they are mostly _end user_ use cases, not IT. The core of GitLab is opensource projects, and it makes sense since it came out as a GitHub competitor. And every feature (meaning lack of) makes sense when looking at it from the opensource perspective.

And yes, we are talking about GitLab runners, especially how they are handled by GitLab.

<!-- more -->

## Context

Runners are basically CI/CD agents. They take jobs from GitLab, execute them, sends the result back to GitLab. All of that in _https_. Nothing new or complex here.

![No seriously, nothing new, nor complex](gitlab_runner_api_proxy/gitlab_api_proxy-01.png)

Runners are great, they feel like a modern take on CI/CD, far from the pioneer that have, to my feeling, aged. And the most useful feature they come with is the ability to execute jobs inside a container, making the CI/CD development and maintenance way, way easier (though it has some drawbacks too). But it's not the point here.

The point is, GitLab offerts _absolutely no way_ to control runners access in a corporate environment, **_even in the entreprise version_**.

## Enrollment process

Here is what happens on runner registering:

1. A project's maintainer (or higher) gets a token from GitLab
2. The runner registers itself with this token, and other options (mainly the runner capabilities, and informations to target it if needed)
3. GitLab accepts the registering, identify the runner, and give this identity to the runner so it can identify itself when requesting jobs

![Enrollment process](gitlab_runner_api_proxy/gitlab_api_proxy-02.png)

The identity is represented by a token. Another token. Let's call it _authentication token_ since it has no name in GitLab. In fact, the other token don't have any name either, so let's clarify them like this:

* The token used for registering will be called _register token_
* The token used as identity will be called _authentication token_

When a runner is registered, it will store the _authentication token_ in its configuration:

```toml
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "docker-runner-direct"
  url = "http://gitlab/"
  token = "_2x24K1frAFiL1gPm_aW"    <-- AUTH TOKEN
  executor = "shell"
```

So, as long as you have a _register token_ you can register _any runner_, _from anywhere_, provided that this anywhere has access to the GitLab instance.

## Security issues

The main issue is the ability to steal the source code and deployment variables when you have a runner connected to the project, even after you have long left the company. But how could you do that?

### Team member lifecycle management flaw

The first and most obvious reason is _you were righteously able to do it at one point_. Project maintainer at one point, you could add any runner, since you add access to the _register token_. Even after you've left, even after you've been removed from the project, your runner will run, as long as nobody has removed it.

### Social engineering flaw

We have said that the _register token_ was available only to owners and maintainers of the project/group. That's quite fine, opensource projects don't usually have many owners/maintainers, and even so, it's opensource, so heh, nothing to steal you know?

But do you know who is owner/maintainer in a project in a corporation? _A lot_ of people. Project managers, technical leaders, developers that have a specific CI/CD task at a moment, and so on.

And... You don't have access to the token, but you need it... just _because_? Ask for it to a maintainer, and here you go. You have the register token. Openbar. Drinks are on me.

### Runner cloning flaw

We have seen that the _authentication token_ is used to identify the runner, and that this token is in the configuration file of the runner. So, what happens when we copy this configuration to another runner? Well, they are two, but for GitLab they are one.

![Runner cloning flaw](gitlab_runner_api_proxy/gitlab_api_proxy-03.png)

When you are in a corp project, tasks are shared. Almost everyone has access to a build agent, for example to update the tooling, to tweak the configuration, and so on. So basically everyone has access to the said _authentication token_.

[GitLab is aware of this issue for at least 3 years](https://gitlab.com/gitlab-org/gitlab-runner/-/commit/b78c7ac66b625fa6d0c5247e5b4afa84a9f5a089). But since their core is opensource oriented, it's not that a big deal for them.

## The idea

The idea is to insert a component between the runners and GitLab, to act as a gateway keeper.

![_The idea_ on requesting](gitlab_runner_api_proxy/gitlab_api_proxy-04.png)

This would solve the issue of duplicated runners. Or at least, detect them, and we could do... _something_. But this would not solve the issue of genuinely added runners, from a wrong person. So we would also need a component acting the same way, but at the registering stage.

![_The idea_ on registering](gitlab_runner_api_proxy/gitlab_api_proxy-05.png)

Also, there would need some _management stuff_ to allow or block an unauthorized runner.

There are a few more requirements for this idea:

* We **don't want** to modify GitLab and/or GitLab runner.
* The solution must be scalable, because our GitLab instance is scalable.

But it raises some questions:

* How do we know that _FOO_ is indeed _FOO_ or not? We feel like the _authentication token_ is part of the solution, but is not enough since it can't ensure that the runner is the original runner.
* How do we authorize or not the registering request, only based on the _register token_?
* The communication protocol used it `https`, will we be able to put some component in the middle?

## The analysis

About `https`, fortunately, we have a scalable instance, with a Load Balancer in front of GitLab Rails instances, and behind the Load Balancer, everything is in `http`, so request are unencrypted. That may not be perfect in term of security, but here, it helps. Or how lowering security may help increasing security.

![_The idea_, hope and expectation](gitlab_runner_api_proxy/gitlab_api_proxy-06.png)

I have installed a `wireshark` on the server running GitLab, and started recording what was happening on http for a while. And it was time for digging.

I quickly managed to find the route used by the gitlab-runner to get its jobs.

![`POST` requests from the runner on `/api/v4/jobs/request` to fetch jobs](gitlab_runner_api_proxy/gitlab_api_proxy-07.png)

A `POST` request to `api/v4/jobs/request` to get jobs. That's something un-documented in GitLab API documentation, but it can be found in the [gitlab-runner source code](https://gitlab.com/gitlab-org/gitlab-runner/-/blob/master/network/gitlab.go#L336). `wireshark` is just easier to find stuff like that.

Let's see what we can find in it. We can find the _authentication token_, as expected. But just behind it, there's this intriguing value: `last_update`.

![`token` and `last_update` values in the runner request](gitlab_runner_api_proxy/gitlab_api_proxy-08.png)

I'm not sure how I made it, but I made the link with the _Last contact_ value, visible in the GitLab runner administration dashboard. Just to make sure, I checked how often and when the value changed, so I kept wireshark running for 2 hours, and checked the change on 2 runners. The _last update_ value in the requests indeed changes when the _Last contact_ value changes in GitLab. So there's definitely a link.

Here is what is saying the [GitLab documentation](https://repository.prace-ri.eu/git/help/user/admin_area/index.md#administering-runners):

_Last contact: Timestamp indicating when the GitLab instance last contacted the runner._

OK. That's weird, because from what I know, runners contact GitLab, not the opposite. But it could mean that GitLab generates the value. That would make sense. If that's the case, then GitLab should give this value to the runner. Let's check the response, just between a `last_update` value change.

Let's filter on the `token` value, and colorize the `last_update` value. And let's have a look at the response to the last request before the `last_update` change.

![Let's see what is in the GitLab response to the runner request](gitlab_runner_api_proxy/gitlab_api_proxy-09.png)

And here it is. The `last_update` value coming from GitLab, only when the value is changed.

![`last_update` value in GitLab response to runner request](gitlab_runner_api_proxy/gitlab_api_proxy-10.png)

That's what we were looking for, at least to detect duplicated runners.

_If the `last_update` value comes from GitLab, as response to a job request, the next request coming from the other clone of the same runner should not be aware of this value, meaning we should be able to see such discrepancies._

Let's find out our theory.

I have started another capture, duplicated a runner, and let everything running for more than one hour. Then, just to speed up things, I have executed a few GitLab-CI jobs on these runners. Then, I have colored every different `last_update` value for the same (authentication) `token` value. And here is what we can find.

First, an isolated `last_update` value. It happens when the duplicated runner is created. It will then receive the correct value, and everything looks normal again.

![`last_update` disruption on first access from duplicated runner](gitlab_runner_api_proxy/gitlab_api_proxy-11.png)

The next use case is when the last_update value is changed. We can see that the second runner comes with the old value after the first runner comes with the new value. Then everything is back to normal.

![`last_update` disruption with duplicated runners](gitlab_runner_api_proxy/gitlab_api_proxy-12.png)

The last use case is after a job execution. It looks like the last_update value is changed in this case too, and we can see the same behavior.

![`last_update` disruption after a job execution](gitlab_runner_api_proxy/gitlab_api_proxy-13.png)

It looks like duplicated runners can be detected that way, and that something can be done, like temporary, or definitive ban of the authentication token, or even force unregistering of these runners in GitLab.

Since this behavior happens because of a race condition, there is still a risk that this pattern does not appear each time the `last_update` value changes.

![`last_update` disruption _won't_ happen everytime](gitlab_runner_api_proxy/gitlab_api_proxy-14.png)

However, the `last_update` value is udated at least every hour, and the more the runner is used, the more frequent the update happens. So we can expect to see this behavior quite often. Also, this behavior happens when a duplicated runner contacts GitLab for the first time.

We have now a solution to, at least, control the runners that are registered. But we still need a solution to control the register process.
