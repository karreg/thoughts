---
authors: [ karreg ]
date: 2021-03-26
categories:
    - development
tags:
    - gitlab
    - access_control
    - security
---

# GitLab runners: more secured registration

We have seen previously [that runners were great, but still not great enough for some corporate environments](gitlab_runner_security_risks.md). We will now see how we can improve things at the registration stage.

<!-- more -->

!!! note
	This (series of) article(s) is a rewrite of [a previous article written in october 2020](gitlab_runner_api_proxy.md).

!!! articles
    _Here is the updated list or articles, in order:_

    * _[GitLab runners security risks](gitlab_runner_security_risks.md)_
    * _[GitLab runners: more secured registration](gitlab_runner_improved_registration.md)_
    * _[GitLab runners: somewhat more secured access control](gitlab_runner_improved_access_control.md)_

!!! update
	In version 16, GitLab has added the possibility to restrict runner registration by allowing pre-registration, and limit registration to administrators. Old behavior seems to be impacted by this change too. This article is thus obsolete, but I keep it anyway.
## One idea

Very soon in the process, we knew that if a solution was possible, it would be with a piece of software between the runners and GitLab, though we were not sure it was possible. This solution would be able to process the requests, and forward them to GitLab, or not, depending on the context. There were a few questions though:

* Are we able to use an intermediate component, even with https, and make it work?
* Do we have the needed information in the request?

Fortunately, we have setup an high availability instance of GitLab, inside a cloud tenant. The load balancer allowed us to redirect requests based on the path.

![The idea, _hope and expectation_](gitlab_runner_improved_registration/gitlab_runner_improved_registration_01.d2)

And the constraints we want to enforce are the following:

* We don't want to modify GitLab source code, for obvious maintenance matter. GitLab is evolving _fast_, and we will never be able to keep up the pace by modifying the source code each and every time
* We don't want to modify GitLab-runner source code, or docker image, or anything else, for the same maintenant matter, but also because we don't know anything about Golang. Finally, there's no way to enforce projects to use the modified runner.

## Register protocol analysis

The gitlab runner communication protocol is undocumented, so there's two way to analyze the protocol. The first one is to read the source code. The other one, the one I prefer, the faster one, is to record the communications with wireshark. I have set up a local GitLab instance configured on http, and used this command to record all `http` traffic: `tshark -f 'port http' -w gitlab.pcap`

And here is what we can find.

The first, important thing, is that we have access to valuable information in the request header. This was a mandatory check before going further, as GitLab is in general configured on https.

The second thing is that, as expected since [the register step is documented](https://docs.gitlab.com/ee/api/runners.html#register-a-new-runner), the register command will send a `POST` request to `/api/v4/runners`, so we can filter this to find out the content of this request.

![_Register request content_](gitlab_runner_improved_registration/gitlab_runner_improved_registration_02.png)

And still as expected, we can find multiple information:

* The register token
* The GitLab runner version
* All the informations given as parameter to the register command, like tags, name, description...

Let's work (for now) only with the register token.

## Register gateway logic

Now that we have the needed data, and a pretty good idea on how we can get between the runner and GitLab, let's work on the logic.

And before that, let's take again the issues with the register step:

!!! info
    Issue 1: Register token is long living and easily shared

If this information is not secure enough, it requires either another, secure enough, information, or a control made by a tier component.

The first option would be something like an API gateway. But the issue is that it would break the protocol between the runner and GitLab, and that breaks one of our constraints.

The second option looks like something more adapted. Let's see what we can do with that and the following logic we would like.

![_Trust tier idea_](gitlab_runner_improved_registration/gitlab_runner_improved_registration_03.d2)

### Use case 1: registration is not authorized

This one is simple, it would be the default use case:

!!! rule
    As a gateway  
    Given I have no authorization for register token AAA  
    When I receive a register request with token AAA  
    Then I reject the request

### Use case 2: registration is authorized

This rule is the opposite of the previous rule:

!!! rule
    As a gateway  
    Given I have an authorization for register token AAA  
    When I receive a register request with token AAA  
    Then I forward the request to GitLab  
    Then I get the reply from GitLab  
    Then I forward the reply to the runner  

With these two, simple rules, we have a fully functional register gateway. We will need 2 more rules to finish the logic though.

### Use case 3: authorization is valid only once

This rule makes sure that the authorization will work only once. The drawback is that in case of register command failure, we will need as many authorization as registration attempts.

!!! rule
    As a gateway  
    Given I have accepted a registration request for register token AAA  
    Given I have forwarded the reply to the runner  
    Then I invalidate the authorization for register token AAA  

### Use case 4: authorization is valid for a limited time

To keep authorization from staying valid indefinitely until registration has happened, we will add a life time on the authorization

!!! rule
    As a gateway  
    Given I have received an authorization for register token AAA  
    When a given time has passed without registration with the token AAA  
    Then I invalidate the authorization for register token AAA  

## Implementing the request/reply forward

First thing first, we have to try to forward both requests from, and replies to the runner. For faster results with a technology I know will ease integration later on, I went to Python for this POC. This is not something I will use in production though.

I'm the Jon Snow of Python.

In fact I'm better than Jon Snow, for I know that I know nothing.

Anyway, after a few hours of litterature [from StackOverflow](https://stackoverflow.com/questions/6656363/proxying-to-another-web-service-with-flask) and coding, I was able to forward requests quite easily by using [Flask](https://flask.palletsprojects.com), [Requests](https://docs.python-requests.org/en/master/) and this code:

```python
def __get_gitlab_response(incoming_request):
    """ Forwards request to gitlab and return response, or ConnectionError if GitLab is not available """
    gitlab_response = requests.request(
        method =          incoming_request.method,
        url =             incoming_request.url.replace(incoming_request.host_url, app.config['GITLAB_URL']),
        headers =         { key: value for (key, value) in incoming_request.headers if key != 'Host' },
        data =            incoming_request.get_data(),
        cookies =         incoming_request.cookies,
        allow_redirects = False)

    return gitlab_response
```

The reply was as easy to forward:

```python
def __create_forwarded_response(incoming_response):
    """ Creates the reponse to send back from the gitlab response """
    excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']
    headers = [(name, value) for (name, value) in incoming_response.raw.headers.items() if name.lower() not in excluded_headers]
    response = Response(incoming_response.content, incoming_response.status_code, headers)
    return response
```

A bit of encapsulation eases the reading. Also, since GitLab may be down, we handle this case.

```python
def __forward_request(incoming_request):
    """ Forward the request, and forwards the response, or error 504 if GitLab is unavailable """
    try:
        # Forwarding request
        gitlab_response = __get_gitlab_response(incoming_request)

        # Forwarding response
        forwarded_response = __create_forwarded_response(gitlab_response)
        return forwarded_response

    except requests.exceptions.ConnectionError as ex:
        return __create_error_response("GitLab is unavailable", ex)
```

Then we add a listener, or whatever it's called, on the registration route:

```python
### See https://docs.gitlab.com/ee/api/runners.html#register-a-new-runner
### See https://docs.gitlab.com/ee/api/runners.html#delete-a-runner
### See https://docs.gitlab.com/ee/api/runners.html#verify-authentication-for-a-registered-runner
@app.route('/api/v4/runners', methods=['POST'])
def runner_registration():
    """ Capture runner registration """
    return __forward_request(request)
```

Our GitLab is using multiple instances behind a LoadBalancer. Requests are forwarded to any instance, without sticky session. To avoid adding a SPOF that could impact all instances, we will have to duplicate the gateway, with as many instance as gitlab. Once this is done, I have re-routed the register requests to this gateway.

![_Rerouting Runner API to the gateway_](gitlab_runner_improved_registration/gitlab_runner_improved_registration_04.d2)

And it worked.

Of course, it did not at the first try. It still required a few hours for the ignorant me to make it work. But provided my lack of knowledge in Python, it was fast enough.

We now have every register requests sent to our gateway, that in turn forwards the request to gitlab, get the reply, and forward the reply to the runner. Once the runner is registered, requests are sent directly to GitLab, since they use another route (we will see that in another article).

There are a few more things to take care of, because the `/api/v4/runners` route if forwarded to the gateway, but in the code above, I only handle the `POST` case. The logic is simple for other verbs: they are forwarded as well. Only the `POST` verb will need more work.

## Implementing the gateway logic

Now that we have a gateway that lets everything pass through, we can work on the logic. Once again, our architecture is helping.

Since we have multiple gateway instances, we need a shared cache. We will use Redis for that, and the `redis` python library.

![_Sharing data between gateway instances_](gitlab_runner_improved_registration/gitlab_runner_improved_registration_04.d2){target="cache_usage"}

Redis is coming with data lifetime management, we will use it to handle authorization lifetime. In this case, the gateway just needs to do 2 things when a request arrives:

1. Is there an authorization in the shared cache
2. If yes, let the request pass through and remove the authorization

![_Logic with cache_](gitlab_runner_improved_registration/gitlab_runner_improved_registration_06.d2)

This simple behavior handles use cases 1, 2 and 3. Here is the associated code snippet:

```python
def is_registration_allowed(self, register_token):
    """ True if runner can be registered, and remove key """
    if self.__is_connected():
        key = "register:{0}".format(register_token)
        return_value = self.cache.get(key) is not None
        self.cache.delete(key)
        return return_value
    else:
        return False
```

It will look for a key with the following format: `register:TOKEN`, `TOKEN` being the authorized register token.

To handle use case 4, it's all done by inserting the authorization with a lifetime. It's done by a simple call to redis-cli:

```bash
redis-cli set "register:${TOKEN}" "allow" EX 10
```

And that's it. With this simple code we have a fully functional gateway, though it's not qualitative enough for production.

## Performance impact

Since we use a gateway, there's an impact on requests round-trip time. That's why I have added a bit of code to measure this overhead. It's computed with this formula:

![_Custom time and GitLab RTT_](gitlab_runner_improved_registration/gitlab_runner_improved_registration_07.d2)

`overhead = custom_time / gitlab_rtt`

I have applied this formula in the following context:

* GitLab is running in a container
* Redis is running in a container
* The gateway is running in a container
* All containers are running on the same machine

The obtained result in this context is the following:


| Measure          | Value  |
|------------------|--------|
| Mean custom_time | 1.2 ms |
| Mean gitlab_rtt  | 2.5 ms |
| Mean overhead    | ~50%   |

Since only registration requests are impacted by this overhead, it's considered negligible.

## How GitLab could implement it

GitLab is already using Redis, so the current implementation could easily be done in GitLab. In fact, the POC described here is using the same Redis instance as GitLab.

The most difficult would probably to design the associated UX. Here's what I would do.

I would add an instance wide setting to allow these two different behaviors:

1. Open runner registration: standard, and current behavior. You only need a token to register your runner.
2. Admin controlled runner registration: The behavior described in this article. An admin needs to allow your runner registration.

I would also add a group/project setting, visible only in the second behavior, to allow some group/project maintainers to register runners without admin control. This settings could be changed only by an admin.

In term of action, when asked for a runner registration, an admin would go the the group/project CI/CD panel, and click on an `Allow registration` button, visible only to admin, and only in the first behavior, that would open a popup asking for the time duration for the authorization (with a default, editable value).

An evolution could be to allow multiple registrations. In this case, a count value could be stored in Redis, associated with the authorization, and decremented each time a registration happens. But it would need to be triple checked to ensure there's no harmful race condition...

## Next articles

In [the next article](gitlab_runner_improved_access_control.md), we will see what we can do about duplicated runners.
